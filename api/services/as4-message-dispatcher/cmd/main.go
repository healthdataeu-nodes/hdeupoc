package main

import (
	"context"
	"fmt"
	"log/slog"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path"
	"syscall"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/as4-message-dispatcher/core/message"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/as4-message-dispatcher/core/polling"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/as4-message-dispatcher/handlers"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/externalapi"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/logger"

	"gopkg.in/yaml.v3"
)

func main() {
	ctx := context.Background()

	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{AddSource: true, ReplaceAttr: logger.CustomAttr})).
		With("AS4", "MESSAGE-DISPATCHER")

	if err := run(ctx, logger); err != nil {
		logger.ErrorContext(ctx, "startup", "ERROR", err)
		os.Exit(1)
	}
}

func run(ctx context.Context, logger *slog.Logger) error {

	// ===================================================================================
	// Configuration

	confPath := path.Join("config", "conf.yaml")

	cfg := struct {
		Web struct {
			ReadTimeout     time.Duration `yaml:"read_timeout"`
			WriteTimeout    time.Duration `yaml:"write_timeout"`
			IdleTimeout     time.Duration `yaml:"idle_timeout"`
			ShutdownTimeout time.Duration `yaml:"shutdown_timeout"`
			APIHost         string        `yaml:"api_host"`
			EnablePolling   bool          `yaml:"enable_polling"`
			PollingInterval time.Duration `yaml:"polling_interval"`
		} `yaml:"as4_message_dispatcher"`
		Domibus struct {
			Username   string `yaml:"username"`
			Host       string `yaml:"host"`
			DisableTLS bool   `yaml:"disable_tls"`
			Sender     string `yaml:"sender"`
		} `yaml:"domibus"`
		DataDiscovery struct {
			APIHost string `yaml:"api_host"`
		} `yaml:"data_discovery"`
		DataPermit struct {
			APIHost string `yaml:"api_host"`
		} `yaml:"data_permit"`
	}{}

	// ===================================================================================
	// App Starting

	logger.InfoContext(ctx, "starting service")
	defer logger.InfoContext(ctx, "shutdown completed")

	yamlFile, err := os.ReadFile(confPath)
	if err != nil {
		return fmt.Errorf("reading yaml file on %q: %w", confPath, err)
	}

	if err := yaml.Unmarshal(yamlFile, &cfg); err != nil {
		return fmt.Errorf("unmarshaling conf %q: %w", confPath, err)
	}

	logger.InfoContext(ctx, "startup", "config", cfg)

	// ===================================================================================
	// Domibus SOAP client

	logger.InfoContext(ctx, "startup", "status", "initializing Domibus SOAP client")

	domibusScheme := "https"
	if cfg.Domibus.DisableTLS {
		domibusScheme = "http"
	}

	domibusURL := &url.URL{
		Scheme: domibusScheme,
		User:   url.User(cfg.Domibus.Username),
		Host:   cfg.Domibus.Host,
	}

	domibusSOAP, err := domibus.NewSOAP(domibusURL, http.DefaultClient)
	if err != nil {
		return fmt.Errorf("creating domibus soap client: %w", err)
	}

	// ===================================================================================
	// Internal Routes

	logger.InfoContext(ctx, "startup", "status", "initializing internal routes config")

	dataDiscoveryURL := &url.URL{
		Scheme: "http",
		Host:   cfg.DataDiscovery.APIHost,
	}

	dataPermitURL := &url.URL{
		Scheme: "http",
		Host:   cfg.DataPermit.APIHost,
	}

	targetURLs := map[string]*url.URL{
		"data-discovery": dataDiscoveryURL,
		"data-permit":    dataPermitURL,
	}

	// ===================================================================================
	// Message processing support

	logger.InfoContext(ctx, "startup", "status", "initializing message processing support")

	msgCfg := &message.Config{
		HTTPClient:  externalapi.NewHTTPClient(logger),
		DomibusSOAP: domibusSOAP,
		TargetURLs:  targetURLs,
	}

	// ===================================================================================
	// Start API Service

	logger.InfoContext(ctx, "startup", "status", "initializing V1 API support")

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	muxConfig := handlers.NewAPIMuxConfig(logger, msgCfg)
	apiMux := handlers.APIMux(muxConfig)

	serverErrors := make(chan error, 1)

	_, port, err := net.SplitHostPort(cfg.Web.APIHost)
	if err != nil {
		return fmt.Errorf("parsing port from %s: %w", cfg.Web.APIHost, err)
	}

	api := http.Server{
		Addr:         ":" + port,
		Handler:      apiMux,
		ReadTimeout:  cfg.Web.ReadTimeout,
		WriteTimeout: cfg.Web.WriteTimeout,
		IdleTimeout:  cfg.Web.IdleTimeout,
		ErrorLog:     slog.NewLogLogger(logger.Handler(), slog.LevelError),
	}

	go func() {
		logger.InfoContext(ctx, "startup", "status", "api router started", "host", api.Addr)
		serverErrors <- api.ListenAndServe()
	}()

	// ===================================================================================
	// Synchronize messages on startup
	// If polling is disabled, we perform a one-time message synchronization when the application starts.

	if !cfg.Web.EnablePolling {
		logger.InfoContext(ctx, "startup", "status", "performing initial message synchronization...")
		if err := message.Process(ctx, logger, msgCfg); err != nil {
			logger.ErrorContext(ctx, "initial message synchronization", "SYNC-ERROR", err)
		}
	}

	// ===================================================================================
	// Start Polling if enabled

	pollerStop := make(chan struct{})
	pollerErrors := make(chan error, 1)
	if cfg.Web.EnablePolling {
		logger.InfoContext(ctx, "startup", "status", "initializing Polling support")

		go func() {
			logger.InfoContext(ctx, "startup", "status", "polling started")
			pollerErrors <- polling.Run(ctx, logger, cfg.Web.PollingInterval, pollerStop, msgCfg)
		}()
	}

	// ===================================================================================
	// Shutdown

	select {
	case err := <-serverErrors:
		return fmt.Errorf("server error: %w", err)

	case err := <-pollerErrors:
		return fmt.Errorf("poller error: %w", err)

	case sig := <-shutdown:
		logger.InfoContext(ctx, "shutdown", "status", "shutdown started", "signal", sig)
		if cfg.Web.EnablePolling {
			close(pollerStop)
			logger.InfoContext(ctx, "shutdown", "status", "polling stopped")
		}
		defer logger.InfoContext(ctx, "shutdown", "status", "shutdown complete", "signal", sig)

		ctx, cancel := context.WithTimeout(ctx, cfg.Web.ShutdownTimeout)
		defer cancel()

		if err := api.Shutdown(ctx); err != nil {
			api.Close()
			return fmt.Errorf("could not stop server gracefully: %w", err)
		}
	}

	return nil
}
