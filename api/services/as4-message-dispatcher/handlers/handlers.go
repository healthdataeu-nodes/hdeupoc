// Package handlers manages the different versions of the API.
package handlers

import (
	"log/slog"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/as4-message-dispatcher/core/message"
	v1 "code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/as4-message-dispatcher/handlers/v1"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web/mid"

	"github.com/gorilla/mux"
)

// APIMuxConfig contains all the mandatory systems required by handlers.
type APIMuxConfig struct {
	Logger        *slog.Logger
	MessageConfig *message.Config
}

// NewAPIMuxConfig constructs a new mux config.
func NewAPIMuxConfig(logger *slog.Logger, msgCfg *message.Config) APIMuxConfig {
	return APIMuxConfig{
		Logger:        logger,
		MessageConfig: msgCfg,
	}
}

// APIMux constructs a http.Handler with all application routes defined.
func APIMux(cfg APIMuxConfig) *mux.Router {
	router := mux.NewRouter()

	log := mid.Log{
		Logger: cfg.Logger,
	}

	router.Use(log.Middleware)

	v1.Routes(router, v1.Config{
		Logger:        cfg.Logger,
		MessageConfig: cfg.MessageConfig,
	})

	return router
}
