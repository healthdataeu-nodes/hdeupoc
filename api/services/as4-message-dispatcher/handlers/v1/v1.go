// Package v1 contains the full set of handler functions and routes
// supported by the v1 web api.
package v1

import (
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/as4-message-dispatcher/core/message"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/as4-message-dispatcher/handlers/v1/eventgrp"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/as4-message-dispatcher/handlers/v1/healthgrp"

	"github.com/gorilla/mux"
)

// Config contains all the mandatory systems required by handlers.
type Config struct {
	Logger        *slog.Logger
	MessageConfig *message.Config
}

// Routes binds all the version 1 routes.
func Routes(router *mux.Router, cfg Config) {
	mgh := eventgrp.New(cfg.Logger, cfg.MessageConfig)
	router.HandleFunc("/events/process-messages", mgh.ProcessMessages).Methods(http.MethodPost)

	hth := healthgrp.NewHandler(cfg.Logger)
	// Check API
	router.HandleFunc("/readiness", hth.Readiness).Methods(http.MethodGet)
	router.HandleFunc("/liveness", hth.Liveness).Methods(http.MethodGet)
}
