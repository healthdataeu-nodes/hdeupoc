// Package eventgrp maintains the group of handlers for domibus webhook.
package eventgrp

import (
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/as4-message-dispatcher/core/message"
)

type Handler struct {
	logger        *slog.Logger
	messageConfig *message.Config
}

// New constructs a handlers for a route access.
func New(log *slog.Logger, msgCfg *message.Config) *Handler {
	return &Handler{
		logger:        log,
		messageConfig: msgCfg,
	}
}

// ProcessMessages process messages from domibus when a webhook event is received.
func (h *Handler) ProcessMessages(w http.ResponseWriter, r *http.Request) {

	if err := message.Process(r.Context(), h.logger, h.messageConfig); err != nil {
		h.logger.Error("webhook-notification", "process-messages", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
