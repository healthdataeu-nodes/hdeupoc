// Package message provides functionality for retrieving and handling messages using both
// polling and webhook-triggered methods.
package message

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
	"net/url"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
)

// Config contains all the mandatory systems required for processing a message.
type Config struct {
	HTTPClient  *http.Client
	DomibusSOAP *domibus.SOAP
	TargetURLs  map[string]*url.URL
}

// Process checks incoming Domibus messages, reads their content, and dispatches them to
// the appropriate URL based on the message type.
func Process(ctx context.Context, logger *slog.Logger, cfg *Config) error {
	resp, err := cfg.DomibusSOAP.ListPendingMsgRequest(ctx, "", domibus.Party{})
	if err != nil {
		return fmt.Errorf("retrieving list pending messages: %w", err)
	}

	logger.Info("checking-messages...",
		slog.Int("message-found", len(resp.MessageID)),
	)

	for _, msgID := range resp.MessageID {
		if msgID == nil {
			logger.Error("checking-message",
				slog.String("messageID:", "NULL"),
			)
			continue
		}

		msg, err := cfg.DomibusSOAP.RetrieveMsgRequest(ctx, msgID)
		if err != nil {
			return fmt.Errorf("retrieve message: %w", err)
		}

		logger.Info("reading-message",
			slog.String("messageID:", *msgID),
		)

		party, err := domibus.RetrieveParty(msg.Messaging)
		if err != nil {
			return fmt.Errorf("retrieve party: %w", err)
		}

		for _, payload := range msg.Payloads {
			domibusMsg, err := domibus.DecodeMessage(string(payload.Value))
			if err != nil {
				return fmt.Errorf("decode message: %w", err)
			}

			endpoint := cfg.TargetURLs[domibusMsg.DataType]
			endpoint.Path = domibusMsg.OperationType

			md := domibus.MessageDispatch{
				Party:     party,
				MessageID: *msgID,
				Payload:   domibusMsg.Payload,
			}

			payload, err := json.Marshal(md)
			if err != nil {
				return fmt.Errorf("marshaling a MessageDispatch")
			}

			if err := dispatch(ctx, cfg.HTTPClient, endpoint, payload); err != nil {
				return fmt.Errorf("dispatch message: %w", err)
			}
		}
	}

	return nil
}

// dispatch sends the payload of a domibus message to the given URL.
func dispatch(ctx context.Context, httpClient *http.Client, endpoint *url.URL, msg []byte) error {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, endpoint.String(), bytes.NewBuffer(msg))
	if err != nil {
		return fmt.Errorf("create a new request: %w", err)
	}

	req.Header.Set("Content-Type", "application/json")

	_, err = httpClient.Do(req)
	if err != nil {
		return fmt.Errorf("doing request: %w", err)
	}

	return nil
}
