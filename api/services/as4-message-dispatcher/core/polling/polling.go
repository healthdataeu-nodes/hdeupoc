// Package polling contains polls functionalities of a domibus server.
package polling

import (
	"context"
	"log/slog"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/as4-message-dispatcher/core/message"
)

func Run(ctx context.Context, logger *slog.Logger, timeInterval time.Duration, stop chan struct{}, msgCfg *message.Config) error {
	ticker := time.NewTicker(timeInterval)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-stop:
			return nil
		case <-ticker.C:
			if err := message.Process(ctx, logger, msgCfg); err != nil {
				logger.Error("polling", "PROCESS-ERROR", err)
			}
		}
	}
}
