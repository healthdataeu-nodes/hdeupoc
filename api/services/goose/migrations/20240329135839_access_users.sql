-- +goose Up
-- +goose StatementBegin
CREATE SCHEMA IF NOT EXISTS access;

CREATE TABLE IF NOT EXISTS access.users (
    user_id         UUID        PRIMARY KEY,
    name            TEXT        NOT NULL,
    api_key_hash    TEXT        NOT NULL,
    date_created    TIMESTAMP   NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_user_api_key_hash ON access.users(api_key_hash);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP SCHEMA IF EXISTS access;
-- +goose StatementEnd
