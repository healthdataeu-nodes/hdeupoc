-- +goose Up
-- +goose StatementBegin
CREATE SCHEMA IF NOT EXISTS track;

CREATE TABLE IF NOT EXISTS track.messages (
		message_id      TEXT        PRIMARY KEY,
		data_type		TEXT		NOT NULL,
		status_code     INT         NOT NULL,
		date_created    TIMESTAMP   NOT NULL,
        date_updated    TIMESTAMP   NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP SCHEMA IF EXISTS track;
-- +goose StatementEnd
