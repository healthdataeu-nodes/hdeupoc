// Package permitgrp maintains the group of handlers for permit access.
package permitgrp

import (
	"encoding/json"
	"log/slog"
	"net/http"
	"strconv"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-permit/core/permit"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/trackmsg"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

const (
	// Operation type.
	updateApplicationStatus = "update/application"
)

type Handler struct {
	logger       *slog.Logger
	permitCore   *permit.Core
	trackMsgCore *trackmsg.Core
	domibusSOAP  *domibus.SOAP
	party        domibus.Party
}

// New constructs a handlers for a route access.
func New(logger *slog.Logger, db *sqlx.DB, domibusSOAP *domibus.SOAP, party domibus.Party) *Handler {
	return &Handler{
		logger:       logger,
		permitCore:   permit.NewCore(logger, db),
		trackMsgCore: trackmsg.NewCore(logger, db),
		domibusSOAP:  domibusSOAP,
		party:        party,
	}
}

// Create creates a permit application.
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var md domibus.MessageDispatch
	if err := json.NewDecoder(r.Body).Decode(&md); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("create-permit", "decode-message-dispatch", err)
		return
	}

	var perm permit.Permit
	err := json.Unmarshal(md.Payload, &perm)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("create-permit", "unmarshal-permit", err)
		return
	}

	if err := h.permitCore.Create(r.Context(), perm.Application); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		h.logger.Error("create-permit", "create-application", err)
		return
	}

	h.logger.Info("create-permit", "application", perm)

	w.WriteHeader(http.StatusCreated)
}

// FetchAll gets all the applications permits.
func (h *Handler) FetchAll(w http.ResponseWriter, r *http.Request) {
	permits, err := h.permitCore.FetchAll(r.Context())
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	permitsBinary, err := json.Marshal(permits)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(permitsBinary)
}

// Update updates an application permit.
func (h *Handler) Update(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var aplUpdate permit.ApplicationUpdate
	if err := json.NewDecoder(r.Body).Decode(&aplUpdate); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	pParams := mux.Vars(r)

	aplID, err := strconv.Atoi(pParams["application-id"])
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	apl, err := h.permitCore.Update(r.Context(), aplID, aplUpdate)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse("application update failed"), http.StatusInternalServerError)
		h.logger.Error("update-application", "update-error", err)
		return
	}

	h.logger.Info("update-application", "application", apl)

	perm := permit.New(apl)
	b64Payload, err := permit.CreateContent(updateApplicationStatus, perm)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	domiResponse, err := h.domibusSOAP.SubmitMsgRequest(r.Context(), b64Payload, "", h.party)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	if domiResponse == nil || len(domiResponse.MessageID) == 0 {
		h.logger.Error("update-application", "track-error", "coud not track the message")
		return
	}

	if err := h.trackMsgCore.Create(r.Context(), *domiResponse.MessageID[0], "data-permit",
		http.StatusAccepted); err != nil {
		h.logger.Error("update-application", "create-track-message", err)
		return
	}

	response := struct {
		MessageIDs []*string `json:"message_ids"`
	}{
		MessageIDs: domiResponse.MessageID,
	}

	web.Respond(r.Context(), w, response, http.StatusAccepted)
}
