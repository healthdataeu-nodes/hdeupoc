package trackmsggrp

type ResponseTrackMsg struct {
	ID         string `json:"message_id"`
	StatusCode int    `json:"status_code"`
}
