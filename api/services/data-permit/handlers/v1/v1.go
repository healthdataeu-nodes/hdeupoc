// Package v1 contains the full set of handler functions and routes
// supported by the v1 web api.
package v1

import (
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-permit/handlers/v1/permitgrp"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-permit/handlers/v1/trackmsggrp"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/checkapi"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

// Config contains all the mandatory systems required by handlers.
type Config struct {
	Logger      *slog.Logger
	DB          *sqlx.DB
	DomibusSOAP *domibus.SOAP
	Party       domibus.Party
}

// Routes binds all the version 1 routes.
func Routes(router *mux.Router, cfg Config) {
	pgh := permitgrp.New(cfg.Logger, cfg.DB, cfg.DomibusSOAP, cfg.Party)

	// Gateway routes.
	router.HandleFunc("/applications", pgh.FetchAll).Methods(http.MethodGet)
	router.HandleFunc("/applications/{application-id}", pgh.Update).Methods(http.MethodPut)

	// Message dispatcher routes.
	router.HandleFunc("/create/application", pgh.Create).Methods(http.MethodPost)

	tmgh := trackmsggrp.New(cfg.Logger, cfg.DB)
	router.HandleFunc("/track-messages/{message-id}", tmgh.Retrieve).Methods(http.MethodGet)
	router.HandleFunc("/track-messages/update", tmgh.Update).Methods(http.MethodPost)

	chh := checkapi.NewHandler(cfg.Logger, cfg.DB)
	// Check API
	router.HandleFunc("/readiness", chh.Readiness).Methods(http.MethodGet)
	router.HandleFunc("/liveness", chh.Liveness).Methods(http.MethodGet)
}
