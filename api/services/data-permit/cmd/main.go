package main

import (
	"context"
	"fmt"
	"log/slog"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-permit/handlers"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/logger"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/sqldb"

	"gopkg.in/yaml.v3"
)

func main() {
	ctx := context.Background()

	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{AddSource: true, ReplaceAttr: logger.CustomAttr})).
		With("NATIONAL-CONNECTOR", "DATA-PERMIT")

	if err := run(ctx, logger); err != nil {
		logger.ErrorContext(ctx, "startup", "ERROR", err)
		os.Exit(1)
	}
}

func run(ctx context.Context, logger *slog.Logger) error {

	// ===================================================================================
	// Configuration

	confPath := "config/conf.yaml"

	cfg := struct {
		Web struct {
			ReadTimeout     time.Duration `yaml:"read_timeout"`
			WriteTimeout    time.Duration `yaml:"write_timeout"`
			IdleTimeout     time.Duration `yaml:"idle_timeout"`
			ShutdownTimeout time.Duration `yaml:"shutdown_timeout"`
			APIHost         string        `yaml:"api_host"`
		} `yaml:"data_permit"`
		DB struct {
			Host         string `yaml:"host"`
			MaxIdleConns int    `yaml:"max_idle_conns"`
			MaxOpenConns int    `yaml:"max_open_conns"`
			DisableTLS   bool   `yaml:"disable_tls"`
		} `yaml:"db"`
		Domibus struct {
			Username   string `yaml:"username"`
			Host       string `yaml:"host"`
			DisableTLS bool   `yaml:"disable_tls"`
			Sender     string `yaml:"sender"`
			Recipient  string `yaml:"recipient"`
		} `yaml:"domibus"`
	}{}

	// ===================================================================================
	// App Starting

	logger.InfoContext(ctx, "starting service")
	defer logger.InfoContext(ctx, "shutdown complete")

	yamlFile, err := os.ReadFile(confPath)
	if err != nil {
		return fmt.Errorf("reading yaml file on %q: %w", confPath, err)
	}

	if err := yaml.Unmarshal(yamlFile, &cfg); err != nil {
		return fmt.Errorf("unmarshaling conf %q: %w", confPath, err)
	}

	logger.InfoContext(ctx, "startup", "config", cfg)

	// ===================================================================================
	// Database Support

	logger.InfoContext(ctx, "startup", "status", "initializing database support", "host", cfg.DB.Host)

	db, err := sqldb.Open(sqldb.Config{
		Host:         cfg.DB.Host,
		Schema:       "permit",
		MaxIdleConns: cfg.DB.MaxIdleConns,
		MaxOpenConns: cfg.DB.MaxOpenConns,
		DisableTLS:   cfg.DB.DisableTLS,
	})
	if err != nil {
		return fmt.Errorf("open db: %w", err)
	}

	defer func() {
		logger.InfoContext(ctx, "shutdown", "status", "stoping database support", "host", cfg.DB.Host)
		db.Close()
	}()

	// ===================================================================================
	// Domibus SOAP client

	logger.InfoContext(ctx, "startup", "status", "initializing Domibus SOAP client")

	domibusScheme := "https"
	if cfg.Domibus.DisableTLS {
		domibusScheme = "http"
	}

	domibusURL := &url.URL{
		Scheme: domibusScheme,
		User:   url.User(cfg.Domibus.Username),
		Host:   cfg.Domibus.Host,
	}

	domibusSOAP, err := domibus.NewSOAP(domibusURL, http.DefaultClient)
	if err != nil {
		return fmt.Errorf("creating domibus soap client: %w", err)
	}

	party := domibus.NewParty(cfg.Domibus.Sender, cfg.Domibus.Recipient)

	// ===================================================================================
	// Start API Service

	logger.InfoContext(ctx, "startup", "status", "initializing V1 API support")

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	muxConfig := handlers.NewAPIMuxConfig(logger, db, domibusSOAP, party)
	apiMux := handlers.APIMux(muxConfig)

	serverErrors := make(chan error, 1)

	_, port, err := net.SplitHostPort(cfg.Web.APIHost)
	if err != nil {
		return fmt.Errorf("parsing port from %s: %w", cfg.Web.APIHost, err)
	}

	api := http.Server{
		Addr:         ":" + port,
		Handler:      apiMux,
		ReadTimeout:  cfg.Web.ReadTimeout,
		WriteTimeout: cfg.Web.WriteTimeout,
		IdleTimeout:  cfg.Web.IdleTimeout,
		ErrorLog:     slog.NewLogLogger(logger.Handler(), slog.LevelError),
	}

	go func() {
		logger.InfoContext(ctx, "startup", "status", "api router started", "host", api.Addr)
		serverErrors <- api.ListenAndServe()
	}()

	// ===================================================================================
	// Shutdown

	select {
	case err := <-serverErrors:
		return fmt.Errorf("server error: %w", err)

	case sig := <-shutdown:
		logger.InfoContext(ctx, "shutdown", "status", "shutdown started", "signal", sig)
		defer logger.InfoContext(ctx, "shutdown", "status", "shutdown complete", "signal", sig)

		ctx, cancel := context.WithTimeout(ctx, cfg.Web.ShutdownTimeout)
		defer cancel()

		if err := api.Shutdown(ctx); err != nil {
			api.Close()
			return fmt.Errorf("could not stop server gracefully: %w", err)
		}
	}

	return nil
}
