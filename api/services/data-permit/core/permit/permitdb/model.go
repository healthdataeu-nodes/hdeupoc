package permitdb

import (
	"github.com/google/uuid"
)

type ApplicationDB struct {
	ID        int       `db:"application_id"`
	Status    string    `db:"status"`
	CatalogID uuid.UUID `db:"catalog_id"`
	Form      string    `db:"form"`
}
