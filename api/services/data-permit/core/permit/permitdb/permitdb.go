// Package permitdb contains permit related CRUD functionality.
package permitdb

import (
	"context"
	"fmt"
	"log/slog"

	"github.com/jmoiron/sqlx"
)

// Store manages the set of APIs for permit database access.
type Store struct {
	logger *slog.Logger
	db     *sqlx.DB
}

// NewStore constructs the api for data access.
func NewStore(logger *slog.Logger, db *sqlx.DB) *Store {
	return &Store{
		logger: logger,
		db:     db,
	}
}

// Create adds an application to the database.
func (s *Store) Create(ctx context.Context, aplDB ApplicationDB) error {
	const q = `
	INSERT INTO applications
		(application_id, status, catalog_id, form)
	VALUES
		(:application_id, :status, :catalog_id, :form)`

	_, err := s.db.NamedExecContext(ctx, q, aplDB)
	if err != nil {
		return fmt.Errorf("inserting application: %w", err)
	}

	return nil
}

// Retrieve retrieves an application from the database.
func (s *Store) Retrieve(ctx context.Context, aplID int) (ApplicationDB, error) {
	const q = `
	SELECT * FROM applications
	WHERE application_id = $1`

	var aplDB ApplicationDB
	err := s.db.QueryRowContext(ctx, q, aplID).Scan(&aplDB.ID, &aplDB.Status, &aplDB.CatalogID, &aplDB.Form)
	if err != nil {
		return ApplicationDB{}, fmt.Errorf("retrieving application: %w", err)
	}

	return aplDB, nil
}

// Query gets all applications from the database.
func (s *Store) Query(ctx context.Context) ([]ApplicationDB, error) {
	const q = `
	SELECT * FROM applications`

	rows, err := s.db.QueryContext(ctx, q)
	if err != nil {
		return nil, fmt.Errorf("selecting applications: %w", err)
	}

	defer rows.Close()
	var applicationsDB []ApplicationDB

	for rows.Next() {
		var aplDB ApplicationDB
		if err := rows.Scan(&aplDB.ID, &aplDB.Status, &aplDB.CatalogID, &aplDB.Form); err != nil {
			return nil, fmt.Errorf("scanning application: %w", err)
		}

		applicationsDB = append(applicationsDB, aplDB)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("iterating over results: %w", err)
	}

	return applicationsDB, nil
}

// Update updates a specfic application.
func (s *Store) Update(ctx context.Context, aplID int, newStatus string) error {
	const q = `
	UPDATE applications SET status = $1 WHERE application_id = $2`

	_, err := s.db.ExecContext(ctx, q, newStatus, aplID)
	if err != nil {
		return fmt.Errorf("updating status: %w", err)
	}

	return nil
}
