// Package permit provides a core business API of a permit.
package permit

import (
	"context"
	"fmt"
	"log/slog"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-permit/core/permit/permitdb"

	"github.com/jmoiron/sqlx"
)

const (
	// Used for application status update.
	statusAccepted = "Accepted"
	statusRejected = "Rejected"
)

// New is a factory function to create a Permit.
func New(application Application) Permit {
	return Permit{
		Application: application,
	}
}

// =======================================================================================

// Core manages the set of APIs for permit.
type Core struct {
	store *permitdb.Store
}

// NewCore constructs the permit acces API.
func NewCore(logger *slog.Logger, db *sqlx.DB) *Core {
	str := permitdb.NewStore(logger, db)
	return &Core{
		store: str,
	}
}

// Create creates an application to a permit.
func (c *Core) Create(ctx context.Context, apl Application) error {
	aplDB, err := toApplicationDB(apl)
	if err != nil {
		return fmt.Errorf("converting Application to ApplicationDB: %w", err)
	}

	if err := c.store.Create(ctx, aplDB); err != nil {
		return fmt.Errorf("create application: %w", err)
	}

	return nil
}

// Retrieve application permit.
func (c *Core) Retrieve(ctx context.Context, aplID int) (Application, error) {
	aplDB, err := c.store.Retrieve(ctx, aplID)
	if err != nil {
		return Application{}, fmt.Errorf("retrieve application: %w", err)
	}

	apl, err := toApplication(aplDB)
	if err != nil {
		return Application{}, fmt.Errorf("convert ApplicationDB to Application: %w", err)
	}

	return apl, nil
}

// FetchAll gets all a applications permit.
func (c *Core) FetchAll(ctx context.Context) ([]Application, error) {
	applicationsDB, err := c.store.Query(ctx)
	if err != nil {
		return nil, fmt.Errorf("fetch all applications: %w", err)
	}

	applications, err := toApplications(applicationsDB)
	if err != nil {
		return nil, fmt.Errorf("converting a slice of ApplicationDB to a slice of Applications: %w", err)
	}

	return applications, nil
}

// Update updates an application permit.
func (c *Core) Update(ctx context.Context, aplID int, aplUpdate ApplicationUpdate) (Application, error) {
	if aplUpdate.Status != statusAccepted && aplUpdate.Status != statusRejected {
		return Application{}, fmt.Errorf("status doesn't exist: %q", aplUpdate.Status)
	}

	err := c.store.Update(ctx, aplID, aplUpdate.Status)
	if err != nil {
		return Application{}, fmt.Errorf("update status application: %w", err)
	}

	aplDB, err := c.store.Retrieve(ctx, aplID)
	if err != nil {
		return Application{}, fmt.Errorf("retrieve application: %w", err)
	}

	apl, err := toApplication(aplDB)
	if err != nil {
		return Application{}, fmt.Errorf("converting ApplicationDB to Application: %w", err)
	}

	return apl, nil
}
