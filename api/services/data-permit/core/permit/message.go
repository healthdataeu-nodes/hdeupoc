package permit

import (
	"encoding/json"
	"fmt"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
)

// CreateContent creates a message application permit and returns the message in
// base64.
func CreateContent(operationType string, perm Permit) (string, error) {
	permitBinary, err := json.Marshal(perm)
	if err != nil {
		return "", fmt.Errorf("marshal permit: %w", err)
	}

	msg, err := domibus.NewMessage(domibus.DataPermit, operationType, permitBinary)
	if err != nil {
		return "", fmt.Errorf("creating message permit: %w", err)
	}

	b64Payload, err := domibus.EncodeMessage(msg)
	if err != nil {
		return "", fmt.Errorf("encoding message permit: %w", err)
	}

	return b64Payload, nil
}
