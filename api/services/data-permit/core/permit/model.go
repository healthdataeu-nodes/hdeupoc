package permit

import (
	"encoding/json"
	"fmt"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-permit/core/permit/permitdb"

	"github.com/google/uuid"
)

// An Application contains the data needed to apply for a permit.
type Application struct {
	ID        int       `json:"application_id"`
	Status    string    `json:"status"`
	CatalogID uuid.UUID `json:"catalog_id"`
	Form      any       `json:"form"`
}

// ApplicationUpdate defines what information may be provided to modify an
// existing Application permit.
type ApplicationUpdate struct {
	Status string `jsons:"status"`
	Detail any    `jsons:"detail"`
}

// A Permit is the payload of a message permit.
type Permit struct {
	Application Application `json:"application"`
}

// =======================================================================================

// toApplicationDB coverts an Application into Application of the DB layer.
func toApplicationDB(apl Application) (permitdb.ApplicationDB, error) {
	b, err := json.Marshal(apl.Form)
	if err != nil {
		return permitdb.ApplicationDB{}, fmt.Errorf("marshaling application form: %w", err)
	}

	return permitdb.ApplicationDB{
		ID:        apl.ID,
		Status:    apl.Status,
		CatalogID: apl.CatalogID,
		Form:      string(b),
	}, nil
}

// toApplication converts a DB Application into Application.
func toApplication(aplDB permitdb.ApplicationDB) (Application, error) {
	if len(aplDB.Form) == 0 {
		return Application{
			ID:        aplDB.ID,
			Status:    aplDB.Status,
			CatalogID: aplDB.CatalogID,
			Form:      nil,
		}, nil
	}

	var form any
	if err := json.Unmarshal([]byte(aplDB.Form), &form); err != nil {
		return Application{}, fmt.Errorf("unmarshaling ApplicationDB form: %w", err)
	}

	return Application{
		ID:        aplDB.ID,
		Status:    aplDB.Status,
		CatalogID: aplDB.CatalogID,
		Form:      form,
	}, nil
}

// toApplications converts DB Applications into Applications.
func toApplications(aplDBs []permitdb.ApplicationDB) ([]Application, error) {
	applications := make([]Application, len(aplDBs))
	for i, application := range aplDBs {
		application, err := toApplication(application)
		if err != nil {
			return nil, fmt.Errorf("converting ApplicationDB to Application: %w", err)
		}

		applications[i] = application
	}

	return applications, nil
}
