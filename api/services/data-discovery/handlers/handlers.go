// Package handlers manages the different versions of the API.
package handlers

import (
	"log/slog"

	v1 "code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-discovery/handlers/v1"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/shacl"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web/mid"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

// APIMuxConfig contains all the mandatory systems required by handlers.
type APIMuxConfig struct {
	Logger      *slog.Logger
	DB          *sqlx.DB
	DomibusSOAP *domibus.SOAP
	SHACLConfig shacl.Config
	Party       domibus.Party
}

// NewAPIMuxConfig constructs a new mux config.
func NewAPIMuxConfig(logger *slog.Logger, db *sqlx.DB, domibusSOAP *domibus.SOAP,
	shaclConfig shacl.Config, party domibus.Party) APIMuxConfig {
	return APIMuxConfig{
		Logger:      logger,
		DB:          db,
		DomibusSOAP: domibusSOAP,
		SHACLConfig: shaclConfig,
		Party:       party,
	}
}

// APIMux constructs a http.Handler with all application routes defined.
func APIMux(cfg APIMuxConfig) *mux.Router {
	router := mux.NewRouter()

	log := mid.Log{
		Logger: cfg.Logger,
	}

	router.Use(log.Middleware)

	v1.Routes(router, v1.Config{
		Logger:      cfg.Logger,
		DB:          cfg.DB,
		DomibusSOAP: cfg.DomibusSOAP,
		SHACLConfig: cfg.SHACLConfig,
		Party:       cfg.Party,
	})

	return router
}
