// Package trackmsggrp maintains the group of handlers for track message access.
package trackmsggrp

import (
	"encoding/json"
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/trackmsg"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

type Handler struct {
	logger       *slog.Logger
	TrackMsgCore *trackmsg.Core
}

// New constructs a handlers for a route access.
func New(logger *slog.Logger, db *sqlx.DB) *Handler {
	return &Handler{
		logger:       logger,
		TrackMsgCore: trackmsg.NewCore(logger, db),
	}
}

// Retrieve retrieve a track message.
func (h *Handler) Retrieve(w http.ResponseWriter, r *http.Request) {
	pParams := mux.Vars(r)
	messageID := pParams["message-id"]

	tm, err := h.TrackMsgCore.RetrieveByMsgID(r.Context(), messageID)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		h.logger.Error("retrieve-track-message", "retrieve", err)
		return
	}

	rawTm, err := json.Marshal(tm)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("retrieve-track-message", "marshal-track-message", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	w.Write(rawTm)
}

// Update updates a track message.
func (h *Handler) Update(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var md domibus.MessageDispatch
	if err := json.NewDecoder(r.Body).Decode(&md); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("update-track-message", "decode-message-dispatch", err)
		return
	}

	var rtm ResponseTrackMsg
	err := json.Unmarshal(md.Payload, &rtm)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		h.logger.Error("update-track-message", "unmarshal-update-track-message", err)
		return
	}

	if _, err := h.TrackMsgCore.Update(r.Context(), rtm.ID, rtm.StatusCode); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		h.logger.Error("update-track-message", "update", err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
