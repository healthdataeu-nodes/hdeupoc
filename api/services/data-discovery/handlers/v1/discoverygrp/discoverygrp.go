// Package discoverygrp maintains the group of handlers for permit access.
package discoverygrp

import (
	"encoding/json"
	"io"
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-discovery/core/discovery"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/dcatap"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/shacl"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/trackmsg"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"

	"github.com/jmoiron/sqlx"
)

const (
	createDataset  = "create/dataset"
	updateDataset  = "update/dataset"
	deleteDataset  = "delete/dataset"
	restoreCatalog = "restore/catalog"
)

type Handler struct {
	logger       *slog.Logger
	trackMsgCore *trackmsg.Core
	domibusSOAP  *domibus.SOAP
	shaclConfig  shacl.Config
	party        domibus.Party
}

// New constructs a handlers for a route access.
func New(logger *slog.Logger, db *sqlx.DB, domibusSOAP *domibus.SOAP, shaclConfig shacl.Config, party domibus.Party) *Handler {
	return &Handler{
		logger:       logger,
		trackMsgCore: trackmsg.NewCore(logger, db),
		domibusSOAP:  domibusSOAP,
		shaclConfig:  shaclConfig,
		party:        party,
	}
}

// CreateDataset creates a dataset.
func (h *Handler) CreateDataset(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := io.ReadAll(r.Body)
	if err != nil {
		h.logger.Error("create-dataset", "read request body", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	var validation shacl.Validation
	if !h.shaclConfig.DisableValidation {
		validation, err = shacl.Validate(r.Context(), h.shaclConfig, body)
		if err != nil {
			h.logger.Error("create-dataset", "validate dataset", err)

			web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
			return
		}
	}

	if validation.Result == "FAILURE" {
		h.logger.Error("create-dataset", "validation failed", err)
		web.Respond(r.Context(), w, newResponse(nil, validation), http.StatusBadRequest)
		return
	}

	disco := discovery.New(body)
	b64Msg, err := discovery.CreateContent(createDataset, disco)
	if err != nil {
		h.logger.Error("create-dataset", "create message content", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	domiResponse, err := h.domibusSOAP.SubmitMsgRequest(r.Context(), b64Msg, "", h.party)
	if err != nil {
		h.logger.Error("create-dataset", "submit message", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	resp := response{
		MessageIDs: domiResponse.MessageID,
	}

	web.Respond(r.Context(), w, resp, http.StatusAccepted)

	if domiResponse == nil || len(domiResponse.MessageID) == 0 {
		h.logger.Error("create-dataset", "track-error", "coud not track the message")
		return
	}

	if err := h.trackMsgCore.Create(r.Context(), *domiResponse.MessageID[0], "data-discovery",
		http.StatusAccepted); err != nil {
		h.logger.Error("create-dataset", "create-track-message", err)
		return
	}
}

// UpdateDataset updates a dataset.
func (h *Handler) UpdateDataset(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := io.ReadAll(r.Body)
	if err != nil {
		h.logger.Error("update-dataset", "read request body", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	var validation shacl.Validation
	if !h.shaclConfig.DisableValidation {
		validation, err = shacl.Validate(r.Context(), h.shaclConfig, body)
		if err != nil {
			h.logger.Error("update-dataset", "validate dataset", err)
			web.Respond(r.Context(), w, validation, http.StatusBadRequest)
			return
		}
	}

	if validation.Result == "FAILURE" {
		h.logger.Error("update-dataset", "validation failed", err)
		web.Respond(r.Context(), w, newResponse(nil, validation), http.StatusBadRequest)
		return
	}

	disco := discovery.New(body)
	b64Msg, err := discovery.CreateContent(updateDataset, disco)
	if err != nil {
		h.logger.Error("update-dataset", "create message content", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	domiResponse, err := h.domibusSOAP.SubmitMsgRequest(r.Context(), b64Msg, "", h.party)
	if err != nil {
		h.logger.Error("update-dataset", "submit message", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	resp := newResponse(domiResponse.MessageID, validation)

	web.Respond(r.Context(), w, resp, http.StatusAccepted)

	if domiResponse == nil || len(domiResponse.MessageID) == 0 {
		h.logger.Error("update-dataset", "track-error", "coud not track the message")
		return
	}

	if err := h.trackMsgCore.Create(r.Context(), *domiResponse.MessageID[0], "data-discovery",
		http.StatusAccepted); err != nil {
		h.logger.Error("update-dataset", "create-track-message", err)
		return
	}
}

// DeletePostDataset deletes a dataset using by furnishing the whole dataset on the
// request body.
func (h *Handler) DeletePostDataset(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := io.ReadAll(r.Body)
	if err != nil {
		h.logger.Error("delete-dataset", "read request body", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
	}

	disco := discovery.New(body)
	b64Msg, err := discovery.CreateContent(deleteDataset, disco)
	if err != nil {
		h.logger.Error("delete-dataset", "create message content", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	domiResponse, err := h.domibusSOAP.SubmitMsgRequest(r.Context(), b64Msg, "", h.party)
	if err != nil {
		h.logger.Error("delete-dataset", "submit message", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	resp := struct {
		MessageIDs []*string `json:"message_ids"`
	}{
		MessageIDs: domiResponse.MessageID,
	}

	web.Respond(r.Context(), w, resp, http.StatusAccepted)

	if domiResponse == nil || len(domiResponse.MessageID) == 0 {
		h.logger.Error("delete-dataset", "track-error", "coud not track the message")
		return
	}

	if err := h.trackMsgCore.Create(r.Context(), *domiResponse.MessageID[0], "data-discovery",
		http.StatusAccepted); err != nil {
		h.logger.Error("delete-dataset", "create-track-message", err)
		return
	}
}

// DeleteDataset deletes a dataset.
func (h *Handler) DeleteDataset(w http.ResponseWriter, r *http.Request) {
	qParam := r.URL.Query().Get("dct-id")

	rawRDF, err := dcatap.NewCatalogRecord(qParam)
	if err != nil {
		h.logger.Error("delete-dataset", "create catalog record", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	disco := discovery.New(rawRDF)
	b64Msg, err := discovery.CreateContent(deleteDataset, disco)
	if err != nil {
		h.logger.Error("delete-dataset", "create message content", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	domiResponse, err := h.domibusSOAP.SubmitMsgRequest(r.Context(), b64Msg, "", h.party)
	if err != nil {
		h.logger.Error("delete-dataset", "submit message", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	resp := struct {
		MessageIDs []*string `json:"message_ids"`
	}{
		MessageIDs: domiResponse.MessageID,
	}

	web.Respond(r.Context(), w, resp, http.StatusAccepted)

	if domiResponse == nil || len(domiResponse.MessageID) == 0 {
		h.logger.Error("delete-dataset", "track-error", "coud not track the message")
		return
	}

	if err := h.trackMsgCore.Create(r.Context(), *domiResponse.MessageID[0], "data-discovery",
		http.StatusAccepted); err != nil {
		h.logger.Error("delete-dataset", "create-track-message", err)
		return
	}
}

// RestoreCatalog restores a catalog.
func (h *Handler) RestoreCatalog(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := io.ReadAll(r.Body)
	if err != nil {
		h.logger.Error("restore-catalog", "read request body", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	var catalog []string
	if err := json.Unmarshal(body, &catalog); err != nil {
		h.logger.Error("restore-catalog", "unmarshal catalog", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	var validations []shacl.Validation
	if !h.shaclConfig.DisableValidation {
		validations, err = discovery.ValidateCatalog(r.Context(), h.shaclConfig, catalog)
		if err != nil {
			h.logger.Error("restore-catalog", "validate catalog", err)
			web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
			return
		}
	}

	for _, validation := range validations {
		if validation.Result == "FAILURE" {
			h.logger.Error("update-dataset", "validation failed", err)
			web.Respond(r.Context(), w, validations, http.StatusBadRequest)
			return
		}
	}

	disco := discovery.New(body)
	b64Msg, err := discovery.CreateContent(restoreCatalog, disco)
	if err != nil {
		h.logger.Error("restore-catalog", "create message content", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	domiResponse, err := h.domibusSOAP.SubmitMsgRequest(r.Context(), b64Msg, "", h.party)
	if err != nil {
		h.logger.Error("restore-catalogt", "submit message", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	response := struct {
		MessageIDs  []*string          `json:"message_ids"`
		Validations []shacl.Validation `json:"validations"`
	}{
		MessageIDs:  domiResponse.MessageID,
		Validations: validations,
	}

	web.Respond(r.Context(), w, response, http.StatusAccepted)
}

// Validate validates a dataset for a standalone validation.
func (h *Handler) Validate(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := io.ReadAll(r.Body)
	if err != nil {
		h.logger.Error("validate-dataset", "read request body", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	validation, err := shacl.Validate(r.Context(), h.shaclConfig, body)
	if err != nil {
		h.logger.Error("validate-dataset", "validate dataset", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	jsonVResp, err := json.Marshal(validation)
	if err != nil {
		h.logger.Error("validate-dataset", "marshal validation result", err)
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonVResp)
}
