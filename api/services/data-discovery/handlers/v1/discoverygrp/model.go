package discoverygrp

import (
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/shacl"
)

type response struct {
	MessageIDs []*string        `json:"message_ids,omitempty"`
	Validation shacl.Validation `json:"validation,omitempty"`
}

func newResponse(messageIDs []*string, validation shacl.Validation) response {
	return response{
		MessageIDs: messageIDs,
		Validation: validation,
	}
}
