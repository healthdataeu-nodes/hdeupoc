// Package messagegrp maintains the group of handlers for domibus message.
package messagegrp

import (
	"encoding/xml"
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"

	"github.com/gorilla/mux"
)

type Handler struct {
	logger      *slog.Logger
	domibusSOAP *domibus.SOAP
}

// New constructs a handlers for a route access.
func New(log *slog.Logger, domibusSOAP *domibus.SOAP) *Handler {
	return &Handler{
		logger:      log,
		domibusSOAP: domibusSOAP,
	}
}

// Status get messsage status.
func (h *Handler) Status(w http.ResponseWriter, r *http.Request) {
	pParams := mux.Vars(r)
	messageID := pParams["message_id"]

	domiMSGStatus, err := h.domibusSOAP.StatusMsgRequest(r.Context(), messageID)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/soap+xml")
	if err := xml.NewEncoder(w).Encode(domiMSGStatus); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}
}
