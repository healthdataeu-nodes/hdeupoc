// Package v1 contains the full set of handler functions and routes
// supported by the v1 web api.
package v1

import (
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-discovery/handlers/v1/discoverygrp"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-discovery/handlers/v1/messagegrp"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/data-discovery/handlers/v1/trackmsggrp"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/checkapi"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/shacl"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

// Config contains all the mandatory systems required by handlers.
type Config struct {
	Logger      *slog.Logger
	DB          *sqlx.DB
	DomibusSOAP *domibus.SOAP
	SHACLConfig shacl.Config
	Party       domibus.Party
}

// Routes binds all the version 1 routes.
func Routes(router *mux.Router, cfg Config) {
	pgh := discoverygrp.New(cfg.Logger, cfg.DB, cfg.DomibusSOAP, cfg.SHACLConfig, cfg.Party)

	// Gateway routes.
	router.HandleFunc("/datasets/validate", pgh.Validate).Methods(http.MethodPost)

	router.HandleFunc("/datasets", pgh.CreateDataset).Methods(http.MethodPost)
	router.HandleFunc("/datasets/create", pgh.CreateDataset).Methods(http.MethodPost)

	router.HandleFunc("/datasets", pgh.UpdateDataset).Methods(http.MethodPut)
	router.HandleFunc("/datasets/update", pgh.UpdateDataset).Methods(http.MethodPost)

	router.HandleFunc("/datasets", pgh.DeleteDataset).Methods(http.MethodDelete)
	router.HandleFunc("/datasets/delete", pgh.DeletePostDataset).Methods(http.MethodPost)

	router.HandleFunc("/catalogs/restore", pgh.RestoreCatalog).Methods(http.MethodPost)

	// Message dispatcher routes.
	subrouter := router.PathPrefix("/messages").Subrouter()
	mgh := messagegrp.New(cfg.Logger, cfg.DomibusSOAP)
	subrouter.HandleFunc("/status/{message_id}", mgh.Status).Methods(http.MethodGet)

	tmgh := trackmsggrp.New(cfg.Logger, cfg.DB)
	router.HandleFunc("/track-messages/{message-id}", tmgh.Retrieve).Methods(http.MethodGet)
	router.HandleFunc("/track-messages/update", tmgh.Update).Methods(http.MethodPost)

	chh := checkapi.NewHandler(cfg.Logger, cfg.DB)
	// Check API
	router.HandleFunc("/readiness", chh.Readiness).Methods(http.MethodGet)
	router.HandleFunc("/liveness", chh.Liveness).Methods(http.MethodGet)
}
