package discovery

import (
	"encoding/json"
	"fmt"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/edelivery/domibus"
)

// CreateContent creates a message discovery content, this content is encoded in
// base64.
func CreateContent(operationType string, disco Discovery) (string, error) {
	discoBinary, err := json.Marshal(disco)
	if err != nil {
		return "", fmt.Errorf("marshal discover: %w", err)
	}

	msg, err := domibus.NewMessage(domibus.DataDiscovery, operationType, discoBinary)
	if err != nil {
		return "", fmt.Errorf("creating message discovery: %w", err)
	}

	b64Payload, err := domibus.EncodeMessage(msg)
	if err != nil {
		return "", fmt.Errorf("encoding message discovery: %w", err)
	}

	return b64Payload, nil
}
