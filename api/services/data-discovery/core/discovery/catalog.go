package discovery

import (
	"context"
	"encoding/base64"
	"fmt"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/shacl"
)

// The Catalog is not ready for use in the central side and may need to be adapted in the future.
// The Catalog was working on Fair Data Point.

// ValidateCatalog validates multiple datasets, which collectively form a catalog.
func ValidateCatalog(ctx context.Context, cfg shacl.Config, catalog []string) ([]shacl.Validation, error) {
	var validations []shacl.Validation
	for _, encodedDataset := range catalog {
		dataset, err := base64.StdEncoding.DecodeString(encodedDataset)
		if err != nil {
			return nil, fmt.Errorf("decoding encoded dataset: %w", err)
		}

		validation, err := shacl.Validate(ctx, cfg, dataset)
		if err != nil {
			return nil, fmt.Errorf("validate datset: %w", err)
		}

		validations = append(validations, validation)
	}

	return validations, nil
}
