// Package discovery provides a core business API of a discovery.
package discovery

// Discovery contains dataset.
type Discovery struct {
	Dataset string
}

// New is a factory function to create a Discovery.
func New(dataset []byte) Discovery {
	return Discovery{
		Dataset: string(dataset),
	}
}
