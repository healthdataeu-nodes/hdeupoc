package discovery

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewDiscovery(t *testing.T) {

	byteArray := []byte{'t', 'e', 's', 't'}

	type args struct {
		byteArray []byte
	}
	discoveryTests := []struct {
		name     string
		args     args
		expected Discovery
	}{
		{
			name: "should create Discovery with dataset",
			args: args{
				byteArray: byteArray,
			},
			expected: Discovery{
				Dataset: "test",
			},
		},
	}

	for _, tt := range discoveryTests {
		t.Run(tt.name, func(t *testing.T) {
			ch := New(tt.args.byteArray)
			assert.Equal(t, tt.expected, ch)
		})
	}
}
