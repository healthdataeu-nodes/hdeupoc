package main

import (
	"context"
	"fmt"
	"log/slog"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"syscall"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/api-gateway/handlers"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/api-gateway/handlers/v1/reverseproxy"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/logger"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/sqldb"

	"gopkg.in/yaml.v3"
)

func main() {
	ctx := context.Background()

	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{AddSource: true, ReplaceAttr: logger.CustomAttr})).
		With("API", "GATEWAY")

	if err := run(ctx, logger); err != nil {
		logger.ErrorContext(ctx, "startup", "ERROR", err)
		os.Exit(1)
	}
}

func run(ctx context.Context, logger *slog.Logger) error {

	// ===================================================================================
	// Configuration

	confPath := path.Join("config", "conf.yaml")

	cfg := struct {
		Web struct {
			ReadTimeout     time.Duration `yaml:"read_timeout"`
			WriteTimeout    time.Duration `yaml:"write_timeout"`
			IdleTimeout     time.Duration `yaml:"idle_timeout"`
			ShutdownTimeout time.Duration `yaml:"shutdown_timeout"`
			APIHost         string        `yaml:"api_host"`
			DisableTLS      bool          `yaml:"disable_tls"`
			TLSCertFile     string        `yaml:"tls_cert_file"`
			TLSKeyFile      string        `yaml:"tls_key_file"`
		} `yaml:"api_gateway"`
		DB struct {
			Host         string `yaml:"host"`
			MaxIdleConns int    `yaml:"max_idle_conns"`
			MaxOpenConns int    `yaml:"max_open_conns"`
			DisableTLS   bool   `yaml:"disable_tls"`
		} `yaml:"db"`
		DataDiscovery struct {
			APIHost string `yaml:"api_host"`
		} `yaml:"data_discovery"`
		DataPermit struct {
			APIHost string `yaml:"api_host"`
		} `yaml:"data_permit"`
	}{}

	// ===================================================================================
	// App Starting

	logger.InfoContext(ctx, "starting service")
	defer logger.InfoContext(ctx, "shutdown complete")

	yamlFile, err := os.ReadFile(confPath)
	if err != nil {
		return fmt.Errorf("reading yaml file on %q: %w", confPath, err)
	}

	if err := yaml.Unmarshal(yamlFile, &cfg); err != nil {
		return fmt.Errorf("unmarshaling conf %q: %w", confPath, err)
	}

	logger.InfoContext(ctx, "startup", "config", cfg)

	// ===================================================================================
	// Database Support

	logger.InfoContext(ctx, "startup", "status", "initializing database support", "host", cfg.DB.Host)

	db, err := sqldb.Open(sqldb.Config{
		Host:         cfg.DB.Host,
		Schema:       "access",
		MaxIdleConns: cfg.DB.MaxIdleConns,
		MaxOpenConns: cfg.DB.MaxOpenConns,
		DisableTLS:   cfg.DB.DisableTLS,
	})
	if err != nil {
		return fmt.Errorf("open db: %w", err)
	}

	defer func() {
		logger.InfoContext(ctx, "shutdown", "status", "stoping database support", "host", cfg.DB.Host)
		db.Close()
	}()

	// ===================================================================================
	// Reverse Proxies
	// To forward requests to each service through this API Gateway, a dedicated
	// reverse proxy must be created for each service, along with a corresponding route.

	logger.InfoContext(ctx, "startup", "status", "initializing Reverse Proxies")

	dataDiscoveryURL := &url.URL{
		Scheme: "http",
		Host:   cfg.DataDiscovery.APIHost,
	}

	dataPermitURL := &url.URL{
		Scheme: "http",
		Host:   cfg.DataPermit.APIHost,
	}

	rProxyConfig := map[string]reverseproxy.Config{
		"data-discovery": {
			URL:    dataDiscoveryURL,
			RProxy: httputil.NewSingleHostReverseProxy(dataDiscoveryURL),
		},
		"data-permit": {
			URL:    dataPermitURL,
			RProxy: httputil.NewSingleHostReverseProxy(dataPermitURL),
		},
	}

	// ===================================================================================
	// Start API Service

	logger.InfoContext(ctx, "startup", "status", "initializing V1 API support")

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	muxConfig := handlers.NewAPIMuxConfig(logger, db, rProxyConfig)
	apiMux := handlers.APIMux(muxConfig)

	serverErrors := make(chan error, 1)

	_, port, err := net.SplitHostPort(cfg.Web.APIHost)
	if err != nil {
		return fmt.Errorf("parsing port from %s: %w", cfg.Web.APIHost, err)
	}

	api := http.Server{
		Addr:         ":" + port,
		Handler:      apiMux,
		ReadTimeout:  cfg.Web.ReadTimeout,
		WriteTimeout: cfg.Web.WriteTimeout,
		IdleTimeout:  cfg.Web.IdleTimeout,
		ErrorLog:     slog.NewLogLogger(logger.Handler(), slog.LevelError),
	}

	if cfg.Web.DisableTLS {
		go func() {
			logger.InfoContext(ctx, "startup", "status", "api router started", "host", api.Addr)
			serverErrors <- api.ListenAndServe()
		}()
	} else {
		certFile := filepath.Join("/", "etc", "ssl", "localcerts", cfg.Web.TLSCertFile)
		keyFile := filepath.Join("/", "etc", "ssl", "localcerts", cfg.Web.TLSKeyFile)

		go func() {
			logger.InfoContext(ctx, "startup", "status", "api router started with TLS", "host", api.Addr)
			serverErrors <- api.ListenAndServeTLS(certFile, keyFile)
		}()
	}

	// ===================================================================================
	// Shutdown

	select {
	case err := <-serverErrors:
		return fmt.Errorf("server error: %w", err)

	case sig := <-shutdown:
		logger.InfoContext(ctx, "shutdown", "status", "shutdown started", "signal", sig)
		defer logger.InfoContext(ctx, "shutdown", "status", "shutdown complete", "signal", sig)

		ctx, cancel := context.WithTimeout(ctx, cfg.Web.ShutdownTimeout)
		defer cancel()

		if err := api.Shutdown(ctx); err != nil {
			api.Close()
			return fmt.Errorf("could not stop server gracefully: %w", err)
		}
	}

	return nil
}
