package reverseproxy

import (
	"net/http/httputil"
	"net/url"
)

// Config is the configuration needed to create a reverse proxy.
type Config struct {
	URL    *url.URL
	RProxy *httputil.ReverseProxy
}
