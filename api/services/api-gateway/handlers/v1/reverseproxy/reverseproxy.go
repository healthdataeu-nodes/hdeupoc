// Package reverseproxy maintains the reverse proxy handlers.
package reverseproxy

import (
	"log/slog"
	"net/http"
	"net/http/httputil"

	"github.com/gorilla/mux"
)

type Handler struct {
	logger       *slog.Logger
	rProxyConfig map[string]Config
}

// New constructs a handlers for a route access.
func New(logger *slog.Logger, rProxyConfig map[string]Config) *Handler {
	return &Handler{
		logger:       logger,
		rProxyConfig: rProxyConfig,
	}
}

// Forward returns a function that serves as an HTTP handler to forward incoming
// requests to a specified ReverseProxy instance.
func (h *Handler) Forward(apiName string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		cfg := h.rProxyConfig[apiName]

		cfg.RProxy = &httputil.ReverseProxy{
			Rewrite: func(r *httputil.ProxyRequest) {
				r.SetXForwarded()
				r.SetURL(cfg.URL)
			},
		}

		r.URL.Path = mux.Vars(r)["path"]
		cfg.RProxy.ServeHTTP(w, r)
	}
}
