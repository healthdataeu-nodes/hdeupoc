// Package v1 contains the full set of handler functions and routes
// supported by the v1 web api.
package v1

import (
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/api-gateway/core/user"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/api-gateway/handlers/middleware"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/api-gateway/handlers/v1/reverseproxy"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/api-gateway/handlers/v1/usergrp"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/checkapi"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

// Config contains all the mandatory systems required by handlers.
type Config struct {
	Logger       *slog.Logger
	DB           *sqlx.DB
	RProxyConfig map[string]reverseproxy.Config
}

// Routes binds all the version 1 routes.
func Routes(router *mux.Router, cfg Config) {
	auth := &middleware.Auth{
		Core: user.NewCore(cfg.Logger, cfg.DB),
	}

	ugh := usergrp.New(cfg.Logger, cfg.DB)

	// Users with API key.
	router.HandleFunc("/users", auth.Middleware(ugh.Create)).Methods(http.MethodPost)
	router.HandleFunc("/users", auth.Middleware(ugh.DeleteAll)).Methods(http.MethodDelete)

	// Forwarding

	rpgh := reverseproxy.New(cfg.Logger, cfg.RProxyConfig)

	router.HandleFunc("/data-discovery/{path:.*}", auth.Middleware(rpgh.Forward("data-discovery")))
	router.HandleFunc("/data-permit/{path:.*}", auth.Middleware(rpgh.Forward("data-permit")))

	chh := checkapi.NewHandler(cfg.Logger, cfg.DB)
	// Check API
	router.HandleFunc("/readiness", chh.Readiness).Methods(http.MethodGet)
	router.HandleFunc("/liveness", chh.Liveness).Methods(http.MethodGet)
}
