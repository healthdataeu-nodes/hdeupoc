// Package usergrp maintains the group of handlers for user access.
package usergrp

import (
	"encoding/json"
	"log/slog"
	"net/http"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/api-gateway/core/user"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"

	"github.com/jmoiron/sqlx"
)

type Handler struct {
	logger *slog.Logger
	core   *user.Core
}

// New constructs a handlers for a route access.
func New(logger *slog.Logger, db *sqlx.DB) *Handler {
	return &Handler{
		logger: logger,
		core:   user.NewCore(logger, db),
	}
}

// Create an API key for a user.
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	var nu user.NewUser
	if err := json.NewDecoder(r.Body).Decode(&nu); err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusBadRequest)
		return
	}

	usr, err := h.core.Create(r.Context(), nu)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse("user creation failed"), http.StatusInternalServerError)
		h.logger.Error("create-user",
			"create-error", err)
		return
	}

	resp := struct {
		Note string    `json:"note"`
		User user.User `json:"user"`
	}{
		Note: "Please save the API key securely !",
		User: usr,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	respBinary, err := json.Marshal(resp)
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	w.Write(respBinary)
}

// DeleteAll deletes all users.
func (h *Handler) DeleteAll(w http.ResponseWriter, r *http.Request) {
	err := h.core.DeleteAll(r.Context())
	if err != nil {
		web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
