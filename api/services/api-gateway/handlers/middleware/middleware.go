// Package middleware contains different middleware used on the API Gateway.
package middleware

import (
	"net/http"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/api-gateway/core/user"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"
)

// Auth is the struct needed for the authorization.
type Auth struct {
	Core *user.Core
}

// Middleware is the the middleware for the API key authorization.
func (a *Auth) Middleware(handler http.HandlerFunc) http.HandlerFunc {
	f := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		isEmpty, err := a.Core.IsUsersEmpty(ctx)
		if err != nil {
			web.Respond(r.Context(), w, web.NewErrorResponse(err.Error()), http.StatusInternalServerError)
			return
		}

		if !isEmpty {
			apiKey := r.Header.Get("X-API-KEY")
			if err := a.Core.APIKeyValidation(ctx, apiKey); err != nil {
				web.Respond(r.Context(), w, web.NewErrorResponse("invalid API Key"), http.StatusForbidden)
				return
			}
		}

		handler.ServeHTTP(w, r)
	}

	return f
}
