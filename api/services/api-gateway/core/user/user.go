// Package user provides a core business API of a user.
package user

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/api-gateway/core/user/userdb"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

// Core manages the set of APIs for permit.
type Core struct {
	store *userdb.Store
}

// NewCore constructs the permit acces api.
func NewCore(logger *slog.Logger, db *sqlx.DB) *Core {
	str := userdb.NewStore(logger, db)
	return &Core{
		store: str,
	}
}

// Create generates an API key and creates the user.
// Returns the api key of the user.
func (c *Core) Create(ctx context.Context, nu NewUser) (User, error) {
	apikey, err := web.NewAPIKey(33)
	if err != nil {
		return User{}, fmt.Errorf("generate api key: %w", err)
	}

	// Need the same timestamp for the creation and the update
	now := time.Now()

	usr := User{
		ID:          uuid.New(),
		Name:        nu.Name,
		APIKey:      apikey,
		DateCreated: now,
	}

	userDB := toUserDB(usr)

	if err := c.store.Create(ctx, userDB); err != nil {
		return User{}, fmt.Errorf("insert user: %w", err)
	}

	return usr, nil
}

// DeleteAll deletes all users.
func (c *Core) DeleteAll(ctx context.Context) error {
	if err := c.store.DeleteAll(ctx); err != nil {
		return fmt.Errorf("delete all users: %w", err)
	}

	return nil
}

// APIKeyValidation hash the API key and checks if this API Key have is authorized.
func (c *Core) APIKeyValidation(ctx context.Context, apikey string) error {
	apiKeyHash := sha256.Sum256([]byte(apikey))

	valid, err := c.store.APIKeyValidation(ctx, hex.EncodeToString(apiKeyHash[:]))
	if err != nil {
		return fmt.Errorf("validate API key hash: %w", err)
	}

	if !valid {
		return errors.New("invalid API key")
	}

	return nil
}

// IsUsersEmpty checks if the table users is empty.
func (c *Core) IsUsersEmpty(ctx context.Context) (bool, error) {
	isEmpty, err := c.store.IsUsersEmpty(ctx)
	if err != nil {
		return false, fmt.Errorf("check table users if empty: %w", err)
	}

	return isEmpty, nil
}
