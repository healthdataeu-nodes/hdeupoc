// Package userdb contains user related CRUD functionality.
package userdb

import (
	"context"
	"fmt"
	"log/slog"

	"github.com/jmoiron/sqlx"
)

// Store manages the set of APIs for user database access.
type Store struct {
	logger *slog.Logger
	db     *sqlx.DB
}

// NewStore constructs the api for data access.
func NewStore(logger *slog.Logger, db *sqlx.DB) *Store {
	return &Store{
		logger: logger,
		db:     db,
	}
}

// Create create a user to the database.
func (s *Store) Create(ctx context.Context, usrDB UserDB) error {
	const q = `
	INSERT INTO users 
		(user_id, name, api_key_hash, date_created) 
	VALUES
		(:user_id, :name, :api_key_hash, :date_created)`

	_, err := s.db.NamedExecContext(ctx, q, usrDB)
	if err != nil {
		return fmt.Errorf("inserting user: %w", err)
	}

	return nil
}

// DeleteAll deletes all users.
func (s *Store) DeleteAll(ctx context.Context) error {
	const q = `
	DELETE FROM users`

	_, err := s.db.ExecContext(ctx, q)
	if err != nil {
		return fmt.Errorf("deleting all users: %w", err)
	}

	return nil
}

// APIKeyValidation validate a given API Key Hash.
func (s *Store) APIKeyValidation(ctx context.Context, apiKeyHash string) (bool, error) {
	const q = `
	SELECT EXISTS (
		SELECT 1
		FROM access.users
		WHERE api_key_hash = $1
	)`

	var valid bool
	err := s.db.QueryRowContext(ctx, q, apiKeyHash).Scan(&valid)
	if err != nil {
		return false, fmt.Errorf("query by passkey: %w", err)
	}

	return valid, nil
}

// IsUsersEmpty checks if the table users is empty.
func (s *Store) IsUsersEmpty(ctx context.Context) (bool, error) {
	const q = `
	SELECT COUNT(*) FROM users
	`

	var count int
	err := s.db.QueryRowContext(ctx, q).Scan(&count)
	if err != nil {
		return false, fmt.Errorf("checking table users if empty: %w", err)
	}

	if count == 0 {
		return true, nil
	}

	return false, nil
}
