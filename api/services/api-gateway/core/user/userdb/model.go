package userdb

import (
	"time"

	"github.com/google/uuid"
)

// UserDB represents individual user of the database layer.
type UserDB struct {
	ID          uuid.UUID `db:"user_id"`
	Name        string    `db:"name"`
	APIKeyHash  string    `db:"api_key_hash"`
	DateCreated time.Time `db:"date_created"`
}
