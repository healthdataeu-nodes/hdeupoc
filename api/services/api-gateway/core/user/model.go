package user

import (
	"crypto/sha256"
	"encoding/hex"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/api/services/api-gateway/core/user/userdb"

	"github.com/google/uuid"
)

// User represents individual user.
type User struct {
	ID          uuid.UUID `json:"user_id"`
	Name        string    `json:"user_name"`
	APIKey      string    `json:"api_key"`
	DateCreated time.Time `json:"date_created"`
}

// NewUser contains information needed to create a new user.
type NewUser struct {
	Name string
}

// =======================================================================================

// toUserDB coverts a User into User of the DB layer.
func toUserDB(usr User) userdb.UserDB {
	apiKeyHash := sha256.Sum256([]byte(usr.APIKey))

	return userdb.UserDB{
		ID:          usr.ID,
		Name:        usr.Name,
		APIKeyHash:  hex.EncodeToString(apiKeyHash[:]),
		DateCreated: usr.DateCreated.UTC(),
	}
}
