package shacl

// =======================================================================================
// Those are models of the SHACL API.
// https://www.itb.ec.europa.eu/docs/guides/latest/validatingRDF/index.html#validation-via-rest-web-service-api

type counter struct {
	NROfAssertions int `json:"nrOfAssertions"`
	NROErrors      int `json:"nrOfErrors"`
	NROWarnings    int `json:"nrOfWarnings"`
}

type Detail struct {
	Description string `json:"description,omitempty"`
	Location    string `json:"location,omitempty"`
	Test        string `json:"test,omitempty"`
}

type report struct {
	Detail []Detail `json:"error"`
}

// ValidationResp is the response we received after a validation.
type validationResp struct {
	Result   string  `json:"result"`
	Counters counter `json:"counters"`
	Reports  report  `json:"reports"`
}
