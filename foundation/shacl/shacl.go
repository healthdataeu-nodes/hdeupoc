// Package shacl is about validating dataset files on the shacl API.
package shacl

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/externalapi"
)

type Config struct {
	DisableValidation bool
	Host              string
	ValidationTimeout time.Duration
	HTTPClient        *http.Client
}

// validation represents the request body for a SHACL validation.
type validationReq struct {
	ContentToValidate string `json:"contentToValidate"`
	ContentSyntax     string `json:"contentSyntax"`
	EmbeddingMethod   string `json:"embeddingMethod"`
	ValidationType    string `json:"validationType"`
	ReportSyntax      string `json:"reportSyntax"`
}

// newValidation is the factory function to create a Validation request body.
func newValidationReq(content, contentSyntax, validationType string) validationReq {
	return validationReq{
		ContentToValidate: content,
		ContentSyntax:     contentSyntax,
		ValidationType:    validationType,
		EmbeddingMethod:   "STRING",
		ReportSyntax:      "application/json",
	}
}

type Validation struct {
	Result         string   `json:"result,omitempty"`
	TotalAssertion int      `json:"total_assertion,omitempty"`
	TotalError     int      `json:"total_error,omitempty"`
	TotalWarning   int      `json:"total_warning,omitempty"`
	Details        []Detail `json:"details,omitempty"`
}

func newValidation(vResp validationResp) Validation {
	return Validation{
		Result:         vResp.Result,
		TotalAssertion: vResp.Counters.NROfAssertions,
		TotalError:     vResp.Counters.NROErrors,
		TotalWarning:   vResp.Counters.NROWarnings,
		Details:        vResp.Reports.Detail,
	}
}

// Validate validates a dataset against specific shapes.
func Validate(ctx context.Context, cfg Config, dataset []byte) (Validation, error) {
	ctx, cancel := context.WithTimeout(ctx, cfg.ValidationTimeout)
	defer cancel()

	validationReq := newValidationReq(string(dataset), "application/rdf+xml", "basic")
	rawValidationReq, err := json.Marshal(validationReq)
	if err != nil {
		return Validation{}, fmt.Errorf("marshalling validation: %w", err)
	}

	header := make(http.Header)
	header.Set("Accept", "application/json")
	header.Set("Content-Type", "application/json")

	path, err := url.JoinPath("shacl", "dcat-ap", "api", "validate")
	if err != nil {
		return Validation{}, fmt.Errorf("joining URL path: %w", err)
	}

	shaclURL := &url.URL{
		Scheme: "http",
		Host:   cfg.Host,
		Path:   path,
	}

	shaclAPI := externalapi.New(shaclURL, cfg.HTTPClient, cfg.ValidationTimeout)
	resp, err := shaclAPI.Request(ctx, http.MethodPost, rawValidationReq, header)
	if err != nil {
		return Validation{}, fmt.Errorf("validate with external request: %w", err)
	}

	defer resp.Body.Close()
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return Validation{}, fmt.Errorf("reading response body: %w", err)
	}

	var validationResp validationResp
	if err := json.Unmarshal(respBody, &validationResp); err != nil {
		return Validation{}, fmt.Errorf("validating: %w", err)
	}

	return newValidation(validationResp), nil
}
