package domibus

import (
	"encoding/xml"

	"github.com/hooklift/gowsdl/soap"
)

// CustomMessaging is the custom messaging used by domibus.
type CustomMessaging struct {
	XMLName        xml.Name     `xml:"http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/ Messaging"`
	UserMessage    *UserMessage `xml:"UserMessage,omitempty" json:"UserMessage,omitempty"`
	MustUnderstand bool         `xml:"mustUnderstand,attr,omitempty" json:"mustUnderstand,omitempty"`
}

// =======================================================================================
// Header, Body & Envelope for logs only.

// CustomHeader is the custom soap header who contains the Messaging.
// The header could contain any type of header but in eDelivery the header
// contains messaging.
type CustomHeader struct {
	XMLName   xml.Name         `xml:"http://schemas.xmlsoap.org/soap/envelope/ Header"`
	Messaging *CustomMessaging `xml:"http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/ Messaging"`
}

// CustomBody is the body inside the envelop struct for logs only.
type CustomBody struct {
	XMLName xml.Name `xml:"Body"`
	Value   string   `xml:",innerxml"`
}

// CustomEnvelope is the main structure of a domibus message for logs only.
type CustomEnvelope struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  CustomHeader
	Body    CustomBody
}

// String is the method to print the CustomEnvelop for logs only.
func (ce CustomEnvelope) String() string {
	binaryEnvelope, err := xml.MarshalIndent(ce, "  ", "    ")
	if err != nil {
		return ""
	}

	return string(binaryEnvelope)
}

// =======================================================================================
// Request type.

// A CustomStatusRequest contains the request needed to get the status request
// with no namespace on MessageID.
type CustomStatusRequest struct {
	XMLName   xml.Name              `xml:"eu:statusRequest"`
	XMLNS     string                `xml:"xmlns:eu,attr"`
	MessageID *Max255nonemptystring `xml:"messageID,omitempty" json:"messageID,omitempty"`
}

// A CustomListPendingRequest contains a custom list pending message request
// with no name on the other fields.
type CustomListPendingRequest struct {
	XMLName        xml.Name              `xml:"eu:listPendingMessagesRequest"`
	XMLNS          string                `xml:"xmlns:eu,attr"`
	MessageID      *Max255nonemptystring `xml:"messageId,omitempty" json:"messageId,omitempty"`
	ConversationID *Max255nonemptystring `xml:"conversationId,omitempty" json:"conversationId,omitempty"`
	RefToMessageID *Max255nonemptystring `xml:"refToMessageId,omitempty" json:"refToMessageId,omitempty"`
	FromPartyID    *Max255nonemptystring `xml:"fromPartyId,omitempty" json:"fromPartyId,omitempty"`
	FinalRecipient *Max255nonemptystring `xml:"finalRecipient,omitempty" json:"finalRecipient,omitempty"`
	OriginalSender *Max255nonemptystring `xml:"originalSender,omitempty" json:"originalSender,omitempty"`
	ReceivedFrom   soap.XSDDateTime      `xml:"receivedFrom,omitempty" json:"receivedFrom,omitempty"`
	ReceivedTo     soap.XSDDateTime      `xml:"receivedTo,omitempty" json:"receivedTo,omitempty"`
}

// A CustomRetrieveRequest contains a custom request to retrieve a message.
type CustomRetrieveRequest struct {
	XMLName   xml.Name              `xml:"eu:retrieveMessageRequest"`
	XMLNS     string                `xml:"xmlns:eu,attr"`
	MessageID *Max255nonemptystring `xml:"messageID,omitempty" json:"messageID,omitempty"`
}

// A CustomRetrieveResponse contains a custom response with the headers.
type CustomRetrieveResponse struct {
	XMLName   xml.Name            `xml:"http://eu.domibus.wsplugin/ retrieveMessageResponse"`
	Messaging *CustomMessaging    `xml:"Messaging,omitempty" json:"messaging,omitempty"`
	Bodyload  *LargePayloadType   `xml:"bodyload,omitempty" json:"bodyload,omitempty"`
	Payloads  []*LargePayloadType `xml:"payload,omitempty" json:"payload,omitempty"`
}

// A CustomLargePayloadType contains a custom payload with an empty name space.
type CustomLargePayloadType struct {
	XMLName     xml.Name `xml:",omitempty"`
	Value       []byte   `xml:"value,omitempty" json:"value,omitempty"`
	PayloadID   string   `xml:"payloadId,attr,omitempty" json:"payloadId,omitempty"`
	ContentType string   `xml:"contentType,attr,omitempty" json:"contentType,omitempty"`
}

// A CustomSubmitRequest has a custom payload type.
type CustomSubmitRequest struct {
	XMLName  xml.Name                  `xml:"http://eu.domibus.wsplugin/ submitRequest"`
	Bodyload *CustomLargePayloadType   `xml:"bodyload,omitempty" json:"bodyload,omitempty"`
	Payloads []*CustomLargePayloadType `xml:"payload,omitempty" json:"payload,omitempty"`
}

// =======================================================================================

// ToMax255StringPTR converts a string into Max255nonemptystring, returns nil if
// string empty.
func ToMax255StringPTR(s string) *Max255nonemptystring {
	if s == "" {
		return nil
	}

	m255nes := Max255nonemptystring(s)

	return &m255nes
}

// ToMax1024StringPTR converts a string into Max1024nonemptystring, return nil
// if string empty.
func ToMax1024StringPTR(s string) *Max1024nonemptystring {
	if s == "" {
		return nil
	}

	m1024nes := Max1024nonemptystring(s)

	return &m1024nes
}
