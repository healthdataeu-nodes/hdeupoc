package domibus

import (
	"fmt"
	"net/http"
	"net/url"
	"sync"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/auth"
	"github.com/hooklift/gowsdl/soap"
)

// SOAP represents the configuration for a Domibus SOAP client.
type SOAP struct {
	Client *soap.Client
	mu     sync.Mutex
}

// NewSOAP creates a new instance of a Domibus SOAP client.
// It initializes the SOAP client with the provided Domibus URL and HTTP client.
// The returned SOAP instance is ready to be used for interacting with Domibus services.
func NewSOAP(domibusURL *url.URL, httpClient *http.Client) (*SOAP, error) {
	password, err := auth.PassFile(domibusURL.User.Username(), domibusURL.Hostname(), domibusURL.Port(), ".domibuspass")
	if err != nil {
		return nil, fmt.Errorf("creating soap auth option: %w", err)
	}

	soapAuth := soap.WithBasicAuth(domibusURL.User.Username(), password)

	http := soap.WithHTTPClient(httpClient)
	domibusURL.Path, err = url.JoinPath("domibus", "services", "wsplugin")
	if err != nil {
		return nil, fmt.Errorf("joining domibus path for soap client: %w", err)
	}

	return &SOAP{
		Client: soap.NewClient(domibusURL.String(), soapAuth, http),
		mu:     sync.Mutex{},
	}, nil
}
