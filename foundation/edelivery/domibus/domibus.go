// Package domibus contains additional functions for the generated domibus_gen.
package domibus

import (
	"context"
	"fmt"
)

// Pluginer contains functionnalities to connect with the domibus server.
type Pluginer interface {
	WebServicePluginInterface

	GetStatusCtx(ctx context.Context, request *CustomStatusRequest) (*MessageStatus, error)
	ListPendingMessagesCtx(ctx context.Context, request *CustomListPendingRequest) (*ListPendingMessagesResponse, error)
	RetrieveMessageCtx(ctx context.Context, request *CustomRetrieveRequest) (*CustomRetrieveResponse, error)
	SubmitMessageCtx(ctx context.Context, request *CustomSubmitRequest) (*SubmitResponse, error)
}

// newWSPlugin create a new domibus web service plugin.
func newWSPlugin(soap *SOAP) *webServicePluginInterface {
	return &webServicePluginInterface{
		client: soap.Client,
	}
}

// =======================================================================================

// GetStatusCtx gets a status of a message from domibus.
func (service *webServicePluginInterface) GetStatusCtx(ctx context.Context,
	request *CustomStatusRequest) (*MessageStatus, error) {
	var response MessageStatus
	err := service.client.CallContext(ctx, "''", request, &response)
	if err != nil {
		return nil, fmt.Errorf("getting status: %w", err)
	}

	return &response, nil
}

// ListPendingMessagesCtx lists pending messages from domibus.
func (service *webServicePluginInterface) ListPendingMessagesCtx(ctx context.Context,
	request *CustomListPendingRequest) (*ListPendingMessagesResponse, error) {
	var response ListPendingMessagesResponse
	err := service.client.CallContext(ctx, "''", request, &response)
	if err != nil {
		return nil, fmt.Errorf("listing pending messages: %w", err)
	}

	return &response, nil
}

// RetrieveMessageCtx retrieves a message from domibus
func (service *webServicePluginInterface) RetrieveMessageCtx(ctx context.Context,
	request *CustomRetrieveRequest) (*CustomRetrieveResponse, error) {
	var response CustomRetrieveResponse
	var messaging CustomMessaging
	err := service.client.CallContextWithHeader(ctx, "''", request, &response, &messaging)
	if err != nil {
		return nil, fmt.Errorf("retriving full message: %w", err)
	}

	response.Messaging = &messaging

	return &response, nil
}

// SubmitMessageCtx submits a message to domibus.
func (service *webServicePluginInterface) SubmitMessageCtx(ctx context.Context,
	request *CustomSubmitRequest) (*SubmitResponse, error) {
	var response SubmitResponse
	err := service.client.CallContext(ctx, "''", request, &response)
	if err != nil {
		return nil, fmt.Errorf("submitting message: %w", err)
	}

	return &response, nil
}
