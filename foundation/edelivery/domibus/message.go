package domibus

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"

	"github.com/hooklift/gowsdl/soap"
)

const (
	DataDiscovery = "data-discovery"
	DataPermit    = "data-permit"
)

// MessageDispatch is the struct used by the message dispatcher with the party
// info.
type MessageDispatch struct {
	MessageID string
	Party     Party
	Payload   []byte
}

// A Message represents a domibus message.
type Message struct {
	DataType      string
	OperationType string
	Payload       []byte
}

// NewMessage is a factory function to create a new Message.
func NewMessage(dataType string, operationType string, payload []byte) (Message, error) {
	dataTypes := map[string]struct{}{
		DataDiscovery: {},
		DataPermit:    {},
	}

	if _, ok := dataTypes[dataType]; !ok {
		return Message{}, fmt.Errorf("invalid data type: %q", dataType)
	}

	return Message{
		DataType:      dataType,
		OperationType: operationType,
		Payload:       payload,
	}, nil
}

// EncodeMessage encodes a Message into base64.
func EncodeMessage(msg Message) (string, error) {
	b, err := json.Marshal(msg)
	if err != nil {
		return "", fmt.Errorf("marshalling message: %w", err)
	}

	return base64.StdEncoding.EncodeToString(b), nil
}

// DecodeMessage decodes a base64 code into a Message.
func DecodeMessage(code string) (Message, error) {
	base64, err := base64.StdEncoding.DecodeString(code)
	if err != nil {
		return Message{}, fmt.Errorf("decoding code:%w", err)
	}

	var msg Message
	if err := json.Unmarshal(base64, &msg); err != nil {
		return Message{}, fmt.Errorf("unmarshalling message: %w", err)
	}

	return msg, nil
}

// =======================================================================================

// StatusMsgRequest constructs a get status request and get the status of a
// message.
func (s *SOAP) StatusMsgRequest(ctx context.Context, msgID string) (*MessageStatus, error) {
	wsp := newWSPlugin(s)

	r := CustomStatusRequest{
		XMLName:   xml.Name{},
		XMLNS:     "http://eu.domibus.wsplugin/",
		MessageID: ToMax255StringPTR(msgID),
	}

	ms, err := wsp.GetStatusCtx(ctx, &r)
	if err != nil {
		return nil, fmt.Errorf("get status request: %w", err)
	}

	return ms, nil
}

// ListPendingMsgRequest construct a list pending message request and gets all
// pending messages. Filter can be used to list the desiered messages.
func (s *SOAP) ListPendingMsgRequest(ctx context.Context, refToMessageID string, party Party) (*ListPendingMessagesResponse, error) {
	wsp := newWSPlugin(s)

	r := CustomListPendingRequest{
		XMLName:        xml.Name{},
		XMLNS:          "http://eu.domibus.wsplugin/",
		MessageID:      nil,
		ConversationID: nil,
		RefToMessageID: ToMax255StringPTR(refToMessageID),
		FromPartyID:    ToMax255StringPTR(party.Sender),
		FinalRecipient: nil,
		OriginalSender: nil,
		ReceivedFrom:   soap.XSDDateTime{},
		ReceivedTo:     soap.XSDDateTime{},
	}

	lpm, err := wsp.ListPendingMessagesCtx(ctx, &r)
	if err != nil {
		return nil, fmt.Errorf("list pending messages request: %w", err)
	}

	return lpm, nil
}

// RetrieveMsgRequest construct a retrieve message request and retrieves the
// messages with a specific message ID.
func (s *SOAP) RetrieveMsgRequest(ctx context.Context, msgID *string) (*CustomRetrieveResponse, error) {
	if msgID == nil {
		return nil, errors.New("message ID is nil")
	}

	wsp := newWSPlugin(s)

	r := CustomRetrieveRequest{
		XMLNS:     "http://eu.domibus.wsplugin/",
		MessageID: ToMax255StringPTR(*msgID),
	}

	rfm, err := wsp.RetrieveMessageCtx(ctx, &r)
	if err != nil {
		return nil, fmt.Errorf("retrieve full message request: %w", err)
	}

	return rfm, nil
}

// SubmitMsgRequest constructs a submit message request with a message and send
// it. If replyToMessageID is furnished, it will reply to a specfific message.
func (s *SOAP) SubmitMsgRequest(ctx context.Context, msg string, replyToMessageID string, party Party) (*SubmitResponse, error) {
	wsp := newWSPlugin(s)

	header := newHeader(party, replyToMessageID)

	s.mu.Lock()
	defer s.mu.Unlock()

	wsp.client.SetHeaders(header)

	r := CustomSubmitRequest{
		XMLName: xml.Name{},
		Bodyload: &CustomLargePayloadType{
			Value:       nil,
			PayloadID:   "",
			ContentType: "",
		},
		Payloads: []*CustomLargePayloadType{
			{
				Value:       []byte(msg),
				PayloadID:   "cid:message",
				ContentType: "text/xml",
			},
		},
	}

	sr, err := wsp.SubmitMessageCtx(ctx, &r)
	if err != nil {
		return nil, fmt.Errorf("submit message request: %w", err)
	}

	return sr, nil
}
