package domibus

import (
	"errors"

	"github.com/hooklift/gowsdl/soap"
)

// A Party contains the sender and the recipient of a message.
type Party struct {
	Sender    string
	Recipient string
}

// NewParty is a factory function of a party.
func NewParty(sender, recipient string) Party {
	return Party{
		Sender:    sender,
		Recipient: recipient,
	}
}

// =======================================================================================

// newHeader create a Header containing a Messaging for the soap Header.
func newHeader(p Party, refToMessageID string) *CustomHeader {
	messaging := CustomMessaging{
		UserMessage: &UserMessage{
			MessageInfo: &MessageInfo{
				Timestamp:      soap.XSDDateTime{},
				MessageId:      nil,
				RefToMessageId: ToMax255StringPTR(refToMessageID),
			},

			PartyInfo: &PartyInfo{
				From: &From{
					PartyId: &PartyId{
						Value: ToMax255StringPTR(p.Sender),
						Type:  ToMax255StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered"),
					},
					Role: ToMax255StringPTR("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator"),
				},
				To: &To{
					PartyId: &PartyId{
						Value: ToMax255StringPTR(p.Recipient),
						Type:  ToMax255StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered"),
					},
					Role: ToMax255StringPTR("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder"),
				},
			},

			CollaborationInfo: &CollaborationInfo{
				AgreementRef: nil,
				Service: &Service{
					Value: ToMax255StringPTR("bdx:noprocess"),
					Type:  ToMax255StringPTR("tc1"),
				},
				Action:         "TC1Leg1",
				ConversationId: "",
			},

			MessageProperties: &MessageProperties{
				Property: []*Property{
					{
						Value: ToMax1024StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1"),
						Name:  ToMax255StringPTR("originalSender"),
						Type:  nil,
					},
					{
						Value: ToMax1024StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4"),
						Name:  ToMax255StringPTR("finalRecipient"),
						Type:  nil,
					},
				},
			},

			PayloadInfo: &PayloadInfo{
				PartInfo: []*PartInfo{
					{
						PartProperties: &PartProperties{
							Property: []*Property{
								{
									Value: ToMax1024StringPTR("text/xml"),
									Name:  ToMax255StringPTR("MimeType"),
									Type:  nil,
								},
							},
						},
						Href: "cid:message",
					},
				},
			},
			Mpc: "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC",
		},
		MustUnderstand: true,
	}

	return &CustomHeader{
		Messaging: &messaging,
	}
}

// RetrieveParty retrieves the sender and the recipient from a CustomMessaging.
func RetrieveParty(messaging *CustomMessaging) (Party, error) {
	if messaging == nil {
		return Party{}, errors.New("messaging is nil")
	}

	if messaging.UserMessage == nil {
		return Party{}, errors.New("userMessage is nil")
	}

	if messaging.UserMessage.PartyInfo == nil {
		return Party{}, errors.New("partyInfo is nil")
	}

	if messaging.UserMessage.PartyInfo.From == nil {
		return Party{}, errors.New("from is nil")
	}

	if messaging.UserMessage.PartyInfo.From.PartyId == nil {
		return Party{}, errors.New("from partyID is nil")
	}

	if messaging.UserMessage.PartyInfo.From.PartyId.Value == nil {
		return Party{}, errors.New("no sender found")
	}

	sender := string(*messaging.UserMessage.PartyInfo.From.PartyId.Value)

	if messaging.UserMessage.PartyInfo.To == nil {
		return Party{}, errors.New("to is nil")
	}

	if messaging.UserMessage.PartyInfo.To.PartyId == nil {
		return Party{}, errors.New("to partyID is nil")
	}

	if messaging.UserMessage.PartyInfo.To.PartyId.Value == nil {
		return Party{}, errors.New("no recipient found")
	}

	recipient := string(*messaging.UserMessage.PartyInfo.To.PartyId.Value)

	return Party{
		Sender:    sender,
		Recipient: recipient,
	}, nil
}
