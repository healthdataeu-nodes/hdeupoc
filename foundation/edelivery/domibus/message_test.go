package domibus

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewMessage(t *testing.T) {
	byteArray := []byte{'t', 'e', 's', 't'}

	tests := []struct {
		inputByteArray     []byte
		inputDataType      string
		inputOperationType string
		want               Message
	}{
		{inputByteArray: byteArray, inputDataType: "data-discovery", inputOperationType: "update", want: Message{DataType: "data-discovery", OperationType: "update", Payload: byteArray}},
		{inputByteArray: byteArray, inputDataType: "data-permit", inputOperationType: "update", want: Message{DataType: "data-permit", OperationType: "update", Payload: byteArray}},
	}

	for _, tc := range tests {
		msg, _ := NewMessage(tc.inputDataType, tc.inputOperationType, tc.inputByteArray)
		assert.Equal(t, msg, tc.want, "The two messages should be the same.")
	}

}

func TestNewMessageErr(t *testing.T) {
	byteArray := []byte{'t', 'e', 's', 't'}

	_, err := NewMessage("not-existing", "update", byteArray)
	if err == nil {
		t.Error("want error for invalid input")
	}

}
