package domibus

import (
	"testing"

	"github.com/hooklift/gowsdl/soap"
	"github.com/stretchr/testify/assert"
)

func Test_newHeader(t *testing.T) {
	type args struct {
		party          Party
		refToMessageID string
	}
	headerTests := []struct {
		name     string
		args     args
		expected *CustomHeader
	}{
		{
			name: "when the sender and recipient are furnished",
			args: args{
				party: Party{
					Sender:    "sender",
					Recipient: "recipient",
				},
				refToMessageID: "00000000000000000000000000",
			},
			expected: &CustomHeader{
				Messaging: &CustomMessaging{

					UserMessage: &UserMessage{
						MessageInfo: &MessageInfo{
							Timestamp:      soap.XSDDateTime{},
							MessageId:      nil,
							RefToMessageId: ToMax255StringPTR("00000000000000000000000000"),
						},

						PartyInfo: &PartyInfo{
							From: &From{
								PartyId: &PartyId{
									Value: ToMax255StringPTR("sender"),
									Type:  ToMax255StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered"),
								},
								Role: ToMax255StringPTR("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator"),
							},
							To: &To{
								PartyId: &PartyId{
									Value: ToMax255StringPTR("recipient"),
									Type:  ToMax255StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered"),
								},
								Role: ToMax255StringPTR("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder"),
							},
						},

						CollaborationInfo: &CollaborationInfo{
							AgreementRef: nil,
							Service: &Service{
								Value: ToMax255StringPTR("bdx:noprocess"),
								Type:  ToMax255StringPTR("tc1"),
							},
							Action:         "TC1Leg1",
							ConversationId: "",
						},

						MessageProperties: &MessageProperties{
							Property: []*Property{
								{
									Value: ToMax1024StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1"),
									Name:  ToMax255StringPTR("originalSender"),
									Type:  nil,
								},
								{
									Value: ToMax1024StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4"),
									Name:  ToMax255StringPTR("finalRecipient"),
									Type:  nil,
								},
							},
						},

						PayloadInfo: &PayloadInfo{
							PartInfo: []*PartInfo{
								{
									PartProperties: &PartProperties{
										Property: []*Property{
											{
												Value: ToMax1024StringPTR("text/xml"),
												Name:  ToMax255StringPTR("MimeType"),
												Type:  nil,
											},
										},
									},
									Href: "cid:message",
								},
							},
						},
						Mpc: "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC",
					},
					MustUnderstand: true,
				},
			},
		},
		{
			name: "when the recipient is an empty string",
			args: args{
				party: Party{
					Sender:    "sender",
					Recipient: "",
				},
				refToMessageID: "00000000000000000000000001",
			},
			expected: &CustomHeader{
				Messaging: &CustomMessaging{

					UserMessage: &UserMessage{
						MessageInfo: &MessageInfo{
							Timestamp:      soap.XSDDateTime{},
							MessageId:      nil,
							RefToMessageId: ToMax255StringPTR("00000000000000000000000001"),
						},

						PartyInfo: &PartyInfo{
							From: &From{
								PartyId: &PartyId{
									Value: ToMax255StringPTR("sender"),
									Type:  ToMax255StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered"),
								},
								Role: ToMax255StringPTR("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/initiator"),
							},
							To: &To{
								PartyId: &PartyId{
									Value: ToMax255StringPTR(""),
									Type:  ToMax255StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered"),
								},
								Role: ToMax255StringPTR("http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/responder"),
							},
						},

						CollaborationInfo: &CollaborationInfo{
							AgreementRef: nil,
							Service: &Service{
								Value: ToMax255StringPTR("bdx:noprocess"),
								Type:  ToMax255StringPTR("tc1"),
							},
							Action:         "TC1Leg1",
							ConversationId: "",
						},

						MessageProperties: &MessageProperties{
							Property: []*Property{
								{
									Value: ToMax1024StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1"),
									Name:  ToMax255StringPTR("originalSender"),
									Type:  nil,
								},
								{
									Value: ToMax1024StringPTR("urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4"),
									Name:  ToMax255StringPTR("finalRecipient"),
									Type:  nil,
								},
							},
						},

						PayloadInfo: &PayloadInfo{
							PartInfo: []*PartInfo{
								{
									PartProperties: &PartProperties{
										Property: []*Property{
											{
												Value: ToMax1024StringPTR("text/xml"),
												Name:  ToMax255StringPTR("MimeType"),
												Type:  nil,
											},
										},
									},
									Href: "cid:message",
								},
							},
						},
						Mpc: "http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/defaultMPC",
					},
					MustUnderstand: true,
				},
			},
		},
	}

	for _, tt := range headerTests {
		t.Run(tt.name, func(t *testing.T) {
			ch := newHeader(tt.args.party, tt.args.refToMessageID)
			assert.Equal(t, tt.expected, ch)
		})
	}
}
