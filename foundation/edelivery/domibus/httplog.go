package domibus

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io"
	"log/slog"
	"net/http"
)

// httpLog contains mandatory data to create an http client.
type httpLog struct {
	roundTripper http.RoundTripper
	logger       *slog.Logger
}

// NewHTTPClient creates an http client with proper log for domibus request.
// This client read the request body for debugging.
func NewHTTPClient(logger *slog.Logger) *http.Client {
	return &http.Client{
		Transport: httpLog{
			roundTripper: http.DefaultTransport,
			logger:       logger,
		},
	}
}

// RoundTrip implements the RoudTripper interface of the http standard library.
func (h httpLog) RoundTrip(r *http.Request) (*http.Response, error) {
	h.logger.Info("request started",
		slog.Group("soap",
			"method", r.Method,
			"URL", r.URL.String()),
	)

	if r.Body != nil {
		b, err := io.ReadAll(r.Body)
		if err != nil {
			h.logger.Error("request body",
				slog.Group("soap",
					"reading request body", err.Error()),
			)
			return nil, fmt.Errorf("reading soap request body: %w", err)
		}

		r.Body = io.NopCloser(bytes.NewBuffer(b))

		fmt.Println(string(b))

		var envelope CustomEnvelope
		err = xml.Unmarshal(b, &envelope)
		if err != nil {
			return nil, fmt.Errorf("unmarshaling soap request body: %w", err)
		}

		fmt.Println("✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉ ENVELOPE ✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉")
		fmt.Println(envelope)
		fmt.Println("✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉ ENVELOPE ✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉✉")

	}

	resp, err := h.roundTripper.RoundTrip(r)
	if err != nil {
		h.logger.Error("request error",
			slog.Group("soap",
				"error", err.Error()),
		)
		return nil, fmt.Errorf("round tripping soap request: %w", err)
	}

	h.logger.Info("request completed",
		slog.Group("soap",
			"statuscode", resp.Status),
	)

	return resp, nil
}
