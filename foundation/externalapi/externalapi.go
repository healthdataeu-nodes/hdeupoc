// Package externalapi provides functionality to do external calls.
package externalapi

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"net/url"
	"time"
)

// ExternalAPI represents the API that we want to making call to.
type ExternalAPI struct {
	URL        *url.URL
	HTTPClient *http.Client
	Timeout    time.Duration
}

// New is the factory function to create an en external API that we want to interact with.
func New(apiURL *url.URL, httpClient *http.Client, timeout time.Duration) *ExternalAPI {
	return &ExternalAPI{
		URL:        apiURL,
		HTTPClient: httpClient,
		Timeout:    timeout,
	}
}

// Request excutes a request to an external API.
func (ex *ExternalAPI) Request(ctx context.Context, httpMethod string, body []byte, header http.Header) (*http.Response, error) {
	if ex.Timeout == 0 {
		ex.Timeout = time.Duration(15 * time.Second)
	}

	ctx, cancel := context.WithTimeout(ctx, ex.Timeout)
	defer cancel()

	var b *bytes.Buffer
	if len(body) != 0 {
		b = bytes.NewBuffer(body)
	}

	req, err := http.NewRequestWithContext(ctx, httpMethod, ex.URL.String(), b)
	if err != nil {
		return nil, fmt.Errorf("creating a new external request: %w", err)
	}

	req.Header = header

	resp, err := ex.HTTPClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("doing the external request: %w", err)
	}

	return resp, nil
}
