package externalapi

import (
	"log/slog"
	"net/http"
)

// httpLog contains mandatory data to create an http client.
type httpLog struct {
	roundTripper http.RoundTripper
	logger       *slog.Logger
}

// NewHTTPClient creates an http client with proper log for external request.
func NewHTTPClient(logger *slog.Logger) *http.Client {
	return &http.Client{
		Transport: httpLog{
			roundTripper: http.DefaultTransport,
			logger:       logger,
		},
	}
}

// RoundTrip implements the RoudTripper interface of the http standard library.
func (h httpLog) RoundTrip(r *http.Request) (*http.Response, error) {
	h.logger.Info("request started",
		slog.Group("external", "method", r.Method, "URL", r.URL.String()),
	)

	resp, err := h.roundTripper.RoundTrip(r)
	if err != nil {
		h.logger.Error("request error", slog.Group("external", "error", err.Error()))
		return nil, err
	}

	h.logger.Info("request completed", slog.Group("external", "statuscode", resp.Status))

	return resp, nil
}
