// Package logger provides support for initializing the log system.
package logger

import (
	"fmt"
	"log/slog"
	"path/filepath"
)

// CustomAttr converts the file name to just the name.ext when this key/value will
// be logged.
func CustomAttr(groups []string, a slog.Attr) slog.Attr {
	if a.Key == slog.SourceKey {
		if source, ok := a.Value.Any().(*slog.Source); ok {
			v := fmt.Sprintf("%s:%d", filepath.Base(source.File), source.Line)
			return slog.Attr{Key: "file", Value: slog.StringValue(v)}
		}
	}

	return a
}
