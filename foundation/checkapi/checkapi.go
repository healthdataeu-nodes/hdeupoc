// Package checkapi provides readiness and liveness check functions for system health monitoring
// for API with db.
package checkapi

import (
	"context"
	"log/slog"
	"net/http"
	"os"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/sqldb"
	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/web"

	"github.com/jmoiron/sqlx"
)

type Handler struct {
	logger *slog.Logger
	db     *sqlx.DB
}

func NewHandler(logger *slog.Logger, db *sqlx.DB) *Handler {
	return &Handler{
		logger: logger,
		db:     db,
	}
}

// Readiness checks if the database is ready and if not will return a 500 status.
// Do not respond by just returning an error because further up in the call
// stack it will interpret that as a non-trusted error.
func (h *Handler) Readiness(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(r.Context(), time.Second)
	defer cancel()

	status := "ready"
	statusCode := http.StatusOK
	if err := sqldb.StatusCheck(ctx, h.db); err != nil {
		status = "not ready"
		statusCode = http.StatusInternalServerError
		h.logger.Info("readiness failure", "status", "db not ready")
	}

	data := struct {
		Status string `json:"status"`
	}{
		Status: status,
	}

	web.Respond(ctx, w, data, statusCode)
}

// Liveness returns simple status info if the service is alive. If the
// app is deployed to a Kubernetes cluster, it will also return pod, node, and
// namespace details via the Downward API. The Kubernetes environment variables
// need to be set within your Pod/Deployment manifest.
func (h *Handler) Liveness(w http.ResponseWriter, r *http.Request) {
	status := "alive"
	_, err := os.Hostname()
	if err != nil {
		status = "not alive"
	}

	data := struct {
		Status string `json:"status,omitempty"`
	}{
		Status: status,
	}

	// This handler provides a free timer loop.

	web.Respond(r.Context(), w, data, http.StatusOK)
}
