// Package auth provides authentication support.
package auth

import (
	"fmt"

	"github.com/JamMasterVilua/genericpass"
)

// PassFile reads a pass file from home directory to retrieve the pass.
func PassFile(username, hostname, port, fileName string) (string, error) {
	f, err := genericpass.OpenDefault(fileName)
	if err != nil {
		return "", fmt.Errorf("opening %q: %w", fileName, err)
	}

	defer f.Close()
	keyParts := []string{username, hostname, port}
	pass, err := genericpass.PasswordFrom(keyParts, f)
	if err != nil {
		return "", fmt.Errorf("reading pass")
	}

	return pass, nil

}
