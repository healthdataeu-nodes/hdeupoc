package trackmsg

import (
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/trackmsg/trackmsgdb"
)

// A TrackMsg contains the data needed to track a message.
type TrackMsg struct {
	ID          string    `json:"message_id"`
	DataType    string    `json:"data_type"`
	StatusCode  int       `json:"status_code"`
	DateCreated time.Time `json:"date_created"`
	DateUpdated time.Time `json:"date_updated"`
}

// =======================================================================================

// toTrackMsgDB convert a track message into a track message of the DB layer.
func toTrackMsgDB(tm TrackMsg) trackmsgdb.TrackMsgDB {
	return trackmsgdb.TrackMsgDB{
		ID:          tm.ID,
		DataType:    tm.DataType,
		StatusCode:  tm.StatusCode,
		DateCreated: tm.DateCreated,
		DateUpdated: tm.DateUpdated,
	}
}

// toTrackMsg convert a track message of the DB into a track message of the business layer.
func toTrackMsg(tmDB trackmsgdb.TrackMsgDB) TrackMsg {
	return TrackMsg{
		ID:          tmDB.ID,
		DataType:    tmDB.DataType,
		StatusCode:  tmDB.StatusCode,
		DateCreated: tmDB.DateCreated,
		DateUpdated: tmDB.DateUpdated,
	}
}
