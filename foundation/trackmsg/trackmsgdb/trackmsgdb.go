// Package trackmsgdb contains tracking message related CRUD functionality.
package trackmsgdb

import (
	"context"
	"fmt"
	"log/slog"

	"github.com/jmoiron/sqlx"
)

// Store manages the set of APIs for track messages database access.
type Store struct {
	logger *slog.Logger
	db     *sqlx.DB
}

// NewStore constructs the api for data access.
func NewStore(logger *slog.Logger, db *sqlx.DB) *Store {
	return &Store{
		logger: logger,
		db:     db,
	}
}

// Create creates a track message to the database.
func (s *Store) Create(ctx context.Context, tmDB TrackMsgDB) error {
	const q = `
	INSERT INTO track.messages 
		(message_id, data_type, status_code, date_created, date_updated) 
	VALUES
		(:message_id, :data_type, :status_code, :date_created, :date_updated)`

	_, err := s.db.NamedExecContext(ctx, q, tmDB)
	if err != nil {
		return fmt.Errorf("creating track message: %w", err)
	}

	return nil
}

// RetrieveByMsgID retrives a track message from the database.
func (s *Store) RetrieveByMsgID(ctx context.Context, tmID string) (TrackMsgDB, error) {
	const q = `
	SELECT * FROM track.messages 
	WHERE
		message_id = $1`

	var tmDB TrackMsgDB
	err := s.db.QueryRowContext(ctx, q, tmID).Scan(&tmDB.ID, &tmDB.DataType, &tmDB.StatusCode,
		&tmDB.DateCreated, &tmDB.DateUpdated)
	if err != nil {
		return TrackMsgDB{}, fmt.Errorf("retrieving track message: %w", err)
	}

	return tmDB, nil
}

// Update updates a track message to the database.
func (s *Store) Update(ctx context.Context, tmDB TrackMsgDB) error {
	const q = `
	UPDATE track.messages 
	SET 
		status_code = :status_code,
		date_updated = :date_updated
	WHERE message_id = :message_id`

	_, err := s.db.NamedExecContext(ctx, q, tmDB)
	if err != nil {
		return fmt.Errorf("updating track message: %w", err)
	}

	return nil
}
