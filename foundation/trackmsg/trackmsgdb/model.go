package trackmsgdb

import (
	"time"
)

type TrackMsgDB struct {
	ID          string    `db:"message_id"`
	DataType    string    `db:"data_type"`
	StatusCode  int       `db:"status_code"`
	DateCreated time.Time `db:"date_created"`
	DateUpdated time.Time `db:"date_updated"`
}
