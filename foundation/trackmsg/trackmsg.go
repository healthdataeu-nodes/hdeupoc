// Package trackmsg provides a core business API to do a tracking message.
package trackmsg

import (
	"context"
	"fmt"
	"log/slog"
	"time"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/trackmsg/trackmsgdb"

	"github.com/jmoiron/sqlx"
)

// Core manages the set of APIs for track message.
type Core struct {
	store *trackmsgdb.Store
}

// NewCore constructs the track message acces API.
func NewCore(logger *slog.Logger, db *sqlx.DB) *Core {
	str := trackmsgdb.NewStore(logger, db)
	return &Core{
		store: str,
	}
}

// Create creates a track message to a DB.
func (c *Core) Create(ctx context.Context, tmID, dataType string, statusCode int) error {
	now := time.Now()

	tm := TrackMsg{
		ID:          tmID,
		DataType:    dataType,
		StatusCode:  statusCode,
		DateCreated: now,
		DateUpdated: now,
	}

	tmDB := toTrackMsgDB(tm)

	if err := c.store.Create(ctx, tmDB); err != nil {
		return fmt.Errorf("create track message: %w", err)
	}

	return nil
}

// RetrieveByMsgID retrives a track message from a DB.
func (c *Core) RetrieveByMsgID(ctx context.Context, tmID string) (TrackMsg, error) {
	tmDB, err := c.store.RetrieveByMsgID(ctx, tmID)
	if err != nil {
		return TrackMsg{}, fmt.Errorf("retrieve track message: %w", err)
	}

	return toTrackMsg(tmDB), nil
}

// Update updates a track message into a DB.
func (c *Core) Update(ctx context.Context, tmID string, statusCode int) (TrackMsg, error) {
	now := time.Now()

	tmDB := trackmsgdb.TrackMsgDB{
		ID:          tmID,
		StatusCode:  statusCode,
		DateUpdated: now,
	}

	err := c.store.Update(ctx, tmDB)
	if err != nil {
		return TrackMsg{}, fmt.Errorf("retrieve track message: %w", err)
	}

	return toTrackMsg(tmDB), nil
}
