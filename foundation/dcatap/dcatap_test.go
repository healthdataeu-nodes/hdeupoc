package dcatap_test

import (
	"testing"

	"code.europa.eu/healthdataeu-nodes/hdeupoc/foundation/dcatap"
	"github.com/stretchr/testify/assert"
)

func TestNewCatalogRecord(t *testing.T) {
	tests := []struct {
		when       string
		identifier string
		want       []byte
		should     string
		wantErr    bool
	}{
		{
			when:       "when there is no identifier",
			identifier: "",
			want:       nil,
			should:     "should fail to create a CatalogRecord",
			wantErr:    true,
		},
		{
			when:       "when the identifier is not an URI",
			identifier: "550e8400-e29b-41d4-a716-446655440000",
			want:       nil,
			should:     "should fail to create a CatalogRecord",
			wantErr:    true,
		},
		{
			when:       "when the identifier is an URI",
			identifier: "http://nobelprize.org/datasets/dcat#ds1",
			want: []byte(`<rdf:RDF xmlns:edp="https://europeandataportal.eu/voc#" xmlns:dct="http://purl.org/dc/terms/" xmlns:spdx="http://spdx.org/rdf/terms#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:j.0="http://data.europa.eu/88u/ontology/dcatapop#" xmlns:adms="http://www.w3.org/ns/adms#" xmlns:dqv="http://www.w3.org/ns/dqv#" xmlns:vcard="http://www.w3.org/2006/vcard/ns#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:schema="http://schema.org/" xmlns:dcat="http://www.w3.org/ns/dcat#" xmlns:foaf="http://xmlns.com/foaf/0.1/">
  <dcat:CatalogRecord>
    <dct:identifier>http://nobelprize.org/datasets/dcat#ds1</dct:identifier>
  </dcat:CatalogRecord>
</rdf:RDF>`),
			should:  "should create a CatalogRecord",
			wantErr: false,
		},
	}

	t.Log("given the need to create a CatalogRecord")
	for _, tt := range tests {
		t.Run(tt.when, func(t *testing.T) {
			rawRDF, err := dcatap.NewCatalogRecord(tt.identifier)
			if tt.wantErr {
				assert.Error(t, err)
				assert.Nil(t, rawRDF)
				return
			}
			assert.NoError(t, err)
			assert.Equal(t, tt.want, rawRDF)
		})
	}
}
