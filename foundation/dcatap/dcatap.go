// Package dcatap provides DCAT-AP support.
package dcatap

import (
	"encoding/xml"
	"fmt"
)

// Define name sapce
const (
	edpNs    = "https://europeandataportal.eu/voc#"
	dctNs    = "http://purl.org/dc/terms/"
	spdxNs   = "http://spdx.org/rdf/terms#"
	rdfNs    = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	j0Ns     = "http://data.europa.eu/88u/ontology/dcatapop#"
	admsNs   = "http://www.w3.org/ns/adms#"
	dqvNs    = "http://www.w3.org/ns/dqv#"
	vcardNs  = "http://www.w3.org/2006/vcard/ns#"
	skosNs   = "http://www.w3.org/2004/02/skos/core#"
	schemaNs = "http://schema.org/"
	dcatNs   = "http://www.w3.org/ns/dcat#"
	foafNs   = "http://xmlns.com/foaf/0.1/"
)

// CatalogRecord describes the metadata of a dataset.
type CatalogRecord struct {
	XMLName    xml.Name `xml:"dcat:CatalogRecord"`
	Identifier string   `xml:"dct:identifier"`
}

// An RDF represents a DCAT-AP in rdf file.
type RDF struct {
	XMLName       xml.Name      `xml:"rdf:RDF"`
	EDPNs         string        `xml:"xmlns:edp,attr"`
	DCTNs         string        `xml:"xmlns:dct,attr"`
	SPDXNs        string        `xml:"xmlns:spdx,attr"`
	RDFNs         string        `xml:"xmlns:rdf,attr"`
	J0Ns          string        `xml:"xmlns:j.0,attr"`
	ADMSNs        string        `xml:"xmlns:adms,attr"`
	DQVNs         string        `xml:"xmlns:dqv,attr"`
	VCARDNs       string        `xml:"xmlns:vcard,attr"`
	SKOSNs        string        `xml:"xmlns:skos,attr"`
	SCHEMANs      string        `xml:"xmlns:schema,attr"`
	DCATNs        string        `xml:"xmlns:dcat,attr"`
	FOAFNs        string        `xml:"xmlns:foaf,attr"`
	CatalogRecord CatalogRecord `xml:"dcat:CatalogRecord"`
}

// NewCatalogRecord create a catalog record with a furnished dct identifier.
func NewCatalogRecord(identifier string) ([]byte, error) {
	rdf := RDF{
		EDPNs:    edpNs,
		DCTNs:    dctNs,
		SPDXNs:   spdxNs,
		RDFNs:    rdfNs,
		J0Ns:     j0Ns,
		ADMSNs:   admsNs,
		DQVNs:    dqvNs,
		VCARDNs:  vcardNs,
		SKOSNs:   skosNs,
		SCHEMANs: schemaNs,
		DCATNs:   dcatNs,
		FOAFNs:   foafNs,
		CatalogRecord: CatalogRecord{
			Identifier: identifier,
		},
	}

	rawRDF, err := xml.MarshalIndent(rdf, "", "  ")
	if err != nil {
		return nil, fmt.Errorf("marshaling indent rdf: %w", err)
	}

	return rawRDF, nil
}
