// Package web contains a small web framework extension.
package web

import (
	"context"
	"net/http"
)

// A Handler is a type that handles a http request within our small framework
type Handler func(ctx context.Context, w http.ResponseWriter, r *http.Request) error
