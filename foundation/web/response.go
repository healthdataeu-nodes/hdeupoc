package web

import (
	"context"
	"encoding/json"
	"net/http"
)

type ErrorResponse struct {
	Message string `json:"error"`
}

func NewErrorResponse(message string) ErrorResponse {
	return ErrorResponse{Message: message}
}

// Respond converts a Go value to JSON and sends it to the client.
func Respond(ctx context.Context, w http.ResponseWriter, data any, statusCode int) {
	if statusCode == http.StatusNoContent {
		w.WriteHeader(statusCode)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(data)
}
