package mid

import (
	"log/slog"
	"net/http"
)

type Log struct {
	Logger *slog.Logger
}

func (l *Log) Middleware(handler http.Handler) http.Handler {
	f := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		l.Logger.Info("request started",
			slog.String("method", r.Method),
			slog.String("path", r.URL.Path),
		)

		handler.ServeHTTP(w, r)

		l.Logger.Info("request completed",
			slog.String("method", r.Method),
		)
	})

	return f
}
