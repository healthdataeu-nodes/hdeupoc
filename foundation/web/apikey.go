package web

import (
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
)

// NewAPIKey is a factory function to create an api key with a specified length
// betweend 30 and 128 characters.
func NewAPIKey(length int) (string, error) {
	if length < 30 || length > 128 {
		return "", errors.New("length should be betweend 30 and 128")
	}

	b := make([]byte, length)

	_, err := rand.Read(b)
	if err != nil {
		return "", fmt.Errorf("reading random: %w", err)
	}

	apikey := hex.EncodeToString(b)

	return apikey, nil
}
