[![N|Solid](https://ehds2pilot.eu/wp-content/uploads/2022/12/logo.png)](https://ehds2pilot.eu/)
# HealthData@EU POC national-connector

Within the wiki documentation, EHDS will discover technical instructions guiding them through the deployment of essential components to connect to the POC network. To successfully finalize the installation and configuration, you are required to:

- Set up and configure a Domibus server, facilitating the exchange of eDelivery messages.
- Deploy and configure the National Connector, utilizing either a Docker Compose file or a Helm Chart template.
- Consider using Postman. Import the provided collection from the **`postman`** folder to streamline the process with pre-configured requests.

# Disclaimer:
The contents of this Git repository serve as a Proof of Concept (POC) and should be treated as such. Before utilizing any information or code from this repository, it is essential to understand the nature of a POC and its associated limitations. Kindly read the following disclaimer carefully:

1. Experimental Nature: This repository contains experimental code, features, or concepts that are under development and may not be fully functional, optimized, or stable. Users should be aware that the code is in a testing phase and may encounter bugs, errors, or other technical issues.

2. Limited Reliability: The information, code, or documentation in this repository is provided for demonstration and illustrative purposes only. While efforts are made to ensure accuracy, we make no representations or warranties of any kind, express or implied, regarding the completeness, accuracy, reliability, suitability, or availability of the code or its related documentation.

3. Security and Privacy: As a POC, the code in this repository may not have undergone rigorous security testing and may contain vulnerabilities or potential risks. It is advised not to use this code for production or sensitive applications. We disclaim any liability for any loss, damage, or unauthorized access to data that may result from using the code.

4. No Support or Maintenance: This POC repository is not intended for production use, and no ongoing support or maintenance is provided. We may not respond to issues or pull requests promptly or at all.

5. No Legal or Professional Advice: The code and information provided in this repository should not be considered as legal, professional, or expert advice. It is for general informational purposes only. For specific guidance or advice, consult with a qualified professional or expert in the relevant field.

6. No Endorsement: The presence of any third-party content, tools, or links in this repository does not constitute an endorsement or recommendation. We do not take responsibility for the accuracy, reliability, or quality of any third-party content or tools.

7. Modifications and Updates: We reserve the right to modify, update, or remove the contents of this repository, including code, documentation, or any other material, at our discretion without prior notice.

By accessing or using any part of this Git repository, you acknowledge that you have read, understood, and agreed to this disclaimer. If you do not agree with any part of this disclaimer, refrain from using the code or information from this repository.

Please be aware that this POC disclaimer may change without prior notice. You are responsible for reviewing this disclaimer periodically for any updates or revisions.

The contents of this deliverable solely represent the perspectives and responsibilities of the author(s). It should be understood that these views do not necessarily reflect the opinions of the European Commission, the Consumers, Health, Agriculture and Food Executive Agency, or any other body of the European Union. The European Commission and the Agency disclaim any responsibility for the use of the deliverable's contents.

-----------------------------------------------------------------------------------------------------------
**Copyright © 2024** HealthData@EU pilot. All rights reserved. For more information on the project, please see [EHDS2 Project Website](https://www.ehds2pilot.eu/)