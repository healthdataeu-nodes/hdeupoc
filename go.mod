module code.europa.eu/healthdataeu-nodes/hdeupoc

go 1.22.4

require (
	github.com/JamMasterVilua/genericpass v1.0.3
	github.com/google/uuid v1.3.1
	github.com/gorilla/mux v1.8.0
	github.com/hooklift/gowsdl v0.5.0
	github.com/jackc/pgx/v5 v5.5.1
	github.com/jmoiron/sqlx v1.3.5
	github.com/stretchr/testify v1.8.4
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.17 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rogpeppe/go-internal v1.6.1 // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)

replace github.com/hooklift/gowsdl => code.europa.eu/healthdataeu-nodes/gowsdl v0.5.1-0.20231121105251-3e761f905e83
