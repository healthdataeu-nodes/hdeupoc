.DEFAULT_GOAL := up

# ========================================================================================
# Builds all the go binaries.

.PHONY: build
build:
	@echo building binaries...

	@go build -C api/services/api-gateway/cmd
	@go build -C api/services/as4-message-dispatcher/cmd
	@go build -C api/services/data-discovery/cmd
	@go build -C api/services/data-permit/cmd
	
# ========================================================================================
# Clean binaries

.PHONY: clean
clean:
	@echo cleaning binaries

	@go clean all

# ========================================================================================
# Running from docker compose.

.PHONY: up
up: build
	@docker compose --profile local up -d --build

.PHONY: down
down: clean
	@docker compose --profile local down

.PHONE: logs
logs:
	@docker compose --profile local logs

# ========================================================================================